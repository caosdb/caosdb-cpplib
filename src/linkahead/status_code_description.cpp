/*
 * This file is a part of the LinkAhead Project.
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021-2024 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <map>                     // for map
#include <stdexcept>               // for out_of_range
#include <string>                  // for basic_string, string
#include <utility>                 // for pair
#include "linkahead/status_code.h" // for StatusCode, get_status_description

namespace linkahead {

/*
 * The descriptions of the StatusCodes 1-16 are originally taken from
 * https://github.com/grpc/grpc/blob/ce5b4e949fc75ed4c19e9ccfec7dc95c8ee9ae45/doc/statuscodes.md
 * and adapted to our purposes.
 *
 * They are origially released under an Apache-2.0 license in the linked
 * repository. However, the linked file itself does not contain a copyright or
 * license statement and so we have to assume that the copyright holders are an
 * unknown subset of the contributers of that repository:
 *
 * Copyright (C) 2015-2021 unknown gRPC Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
auto get_status_description(int code) -> const std::string & {
  static const std::string MISSING_DESCRIPTION = "MISSING DESCRIPTION";
  static const std::map<int, std::string> descriptions = {
    {StatusCode::INITIAL, "The transaction has just been initialized. It has not been executed yet "
                          "and clients can still change it and add sub-transactions."},
    {StatusCode::GO_ON, "The transaction has a transaction_type yet and clients may add matching "
                        "sub-transaction or execute it right-away."},
    {StatusCode::READY, "The transaction is ready for execution and cannot be changed anymore."},
    {StatusCode::EXECUTING, "The transaction is currently being executed."},
    {StatusCode::SUCCESS, "The action was successful"},

    {StatusCode::CANCELLED, "The operation was canceled (typically by the caller)."},
    {StatusCode::UNKNOWN, "Unknown error. This is typically a bug (server or client)."},
    {StatusCode::INVALID_ARGUMENT,
     "Client specified an invalid argument. Note that this differs from FAILED_PRECONDITION. "
     "INVALID_ARGUMENT indicates arguments that are problematic regardless of the state of the "
     "system (e.g., a malformed file name)."},
    {StatusCode::DEADLINE_EXCEEDED,
     "Deadline expired before operation could complete. For operations that change the state of "
     "the system, this error may be returned even if the operation has completed successfully. For "
     "example, a successful response from a server could have been delayed long enough for the "
     "deadline to expire."},
    {StatusCode::NOT_FOUND, "Some requested entity (e.g., file or directory) was not found."},
    {StatusCode::ALREADY_EXISTS,
     "Some entity that we attempted to create (e.g., file or directory) already exists."},
    {StatusCode::PERMISSION_DENIED,
     "The caller does not have permission to execute the specified operation. PERMISSION_DENIED "
     "must not be used for rejections caused by exhausting some resource (use RESOURCE_EXHAUSTED "
     "instead for those errors). PERMISSION_DENIED must not be used if the caller can not be "
     "identified (use UNAUTHENTICATED instead for those errors)."},
    {StatusCode::RESOURCE_EXHAUSTED, "Some resource has been exhausted, perhaps a per-user quota, "
                                     "or perhaps the entire file system is out of space."},
    {StatusCode::FAILED_PRECONDITION,
     "Operation was rejected because the system is not in a state required for the operation's "
     "execution. For example, directory to be deleted may be non-empty, an rmdir operation is "
     "applied to a non-directory, etc. A litmus test that may help a service implementor in "
     "deciding between FAILED_PRECONDITION, ABORTED, and UNAVAILABLE: (a) Use UNAVAILABLE if the "
     "client can retry just the failing call. (b) Use ABORTED if the client should retry at a "
     "higher-level (e.g., restarting a read-modify-write sequence). (c) Use FAILED_PRECONDITION if "
     "the client should not retry until the system state has been explicitly fixed. E.g., if an "
     "'rmdir' fails because the directory is non-empty, FAILED_PRECONDITION should be returned "
     "since the client should not retry unless they have first fixed up the directory by deleting "
     "files from it. (d) Use FAILED_PRECONDITION if the client performs conditional REST "
     "Get/Update/Delete on a resource and the resource on the server does not match the condition. "
     "E.g., conflicting read-modify-write on the same resource."},
    {StatusCode::ABORTED,
     "The operation was aborted, typically due to a concurrency issue like sequencer check "
     "failures, transaction aborts, etc. See litmus test above for deciding between "
     "FAILED_PRECONDITION, ABORTED, and UNAVAILABLE."},
    {StatusCode::OUT_OF_RANGE,
     "Operation was attempted past the valid range. E.g., seeking or reading past end of file. "
     "Unlike INVALID_ARGUMENT, this error indicates a problem that may be fixed if the system "
     "state changes. For example, a 32-bit file system will generate INVALID_ARGUMENT if asked to "
     "read at an offset that is not in the range [0,2^32-1], but it will generate OUT_OF_RANGE if "
     "asked to read from an offset past the current file size. There is a fair bit of overlap "
     "between FAILED_PRECONDITION and OUT_OF_RANGE. We recommend using OUT_OF_RANGE (the more "
     "specific error) when it applies so that callers who are iterating through a space can easily "
     "look for an OUT_OF_RANGE error to detect when they are done."},
    {StatusCode::UNIMPLEMENTED,
     "Operation is not implemented or not supported/enabled in this service. In contrast to "
     "UNSUPPORTED_FEATURE (which is a client error) this error indicates that the server does not "
     "support this operation."},
    {StatusCode::INTERNAL,
     "Internal errors. Means some invariants expected by underlying System has been broken. If you "
     "see one of these errors, Something is very broken. "},
    {StatusCode::DATA_LOSS, "Unrecoverable data loss or corruption."},

    {StatusCode::CONNECTION_ERROR,
     "The attempt to execute this transaction was not successful because the "
     "connection to the server could not be established."},
    {StatusCode::AUTHENTICATION_ERROR,
     "The attempt to execute this transaction has not been executed at all "
     "because the authentication did not succeed."},
    {StatusCode::GENERIC_RPC_ERROR,
     "The attempt to execute this transaction was not successful because an "
     "error occured in the transport or RPC protocol layer."},
    {StatusCode::GENERIC_ERROR, "An error occured. Please review the logs for more information."},
    {StatusCode::GENERIC_TRANSACTION_ERROR,
     "The transaction terminated unsuccessfully with transaction errors."},
    {StatusCode::CONFIGURATION_ERROR,
     "An error occurred during the configuration of the ConfigurationManager."},
    {StatusCode::CONNECTION_CONFIGURATION_ERROR,
     "Either there is no connection of the given name or the given connection has a faulty "
     "configuration"},
    {StatusCode::TRANSACTION_STATUS_ERROR,
     "The Transaction is in a wrong state for the attempted action."},
    {StatusCode::TRANSACTION_TYPE_ERROR,
     "The Transaction has a transaction type which does not allow the "
     "attempted action."},
    {StatusCode::ORIGINAL_ENTITY_MISSING_ID,
     "The attempt to update this entity failed because this entity does not "
     "have "
     "an id. This is the case when you did not retrieve it before applying any "
     "changes and instantiated the Entity class explicitly."},
    {StatusCode::NOT_A_FILE_ENTITY, "You can only add files to file entities."},
    {StatusCode::PATH_IS_A_DIRECTORY, "The given path is a directory."},
    {StatusCode::FILE_DOES_NOT_EXIST_LOCALLY,
     "The file does not not exist in the local file system."},
    {StatusCode::FILE_DOWNLOAD_ERROR, "The transaction failed during the download of the files"},
    {StatusCode::FILE_UPLOAD_ERROR, "The transaction failed during the upload of the files"},
    {StatusCode::UNSUPPORTED_FEATURE,
     "This feature is not available in the this client implementation."},
    //{StatusCode::EXTERN_C_ASSIGNMENT_ERROR,
    //"You tried to assign a new object to the wrapped void pointer. You have "
    //"to delete the old pointee first."},
    {StatusCode::ENUM_MAPPING_ERROR,
     "The role, importance, or datatype you specified does not exist."},
    {StatusCode::SPOILED,
     "The object has been used in a way that left it in a corrupted state. This does not indicate "
     "any form of misuse. It just indicates that the object is spoiled for further use now."},
    {StatusCode::OTHER_CLIENT_ERROR,
     "This is code is reserved to errors raised by other clients wrapping the "
     "C++ client (or its Extern C interface).  This should never occur when "
     "working with the C++ code itself."}};
  try {
    return descriptions.at(code);
  } catch (const std::out_of_range &exc) {
    return MISSING_DESCRIPTION;
  }
}

} // namespace linkahead

//  LocalWords:  ConnectionManager Extern
