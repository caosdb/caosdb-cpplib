/*
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021-2024 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
#include "linkahead/authentication.h"
#include <grpcpp/security/auth_context.h> // for AuthContext
#include <grpcpp/security/credentials.h>  // for MetadataCredentialsPlugin
#include <grpcpp/support/status.h>        // for Status
#include <map>                            // for multimap
#include <memory>                         // for allocator, shared_ptr, uni...
#include <string>                         // for basic_string, operator+
#include <utility>                        // for pair, move, make_pair
#include "linkahead/utility.h"            // for base64_encode

namespace linkahead::authentication {
using grpc::AuthContext;
using grpc::MetadataCredentialsPlugin;
using grpc::Status;
using grpc::string_ref;
using linkahead::utility::base64_encode;

MetadataCredentialsPluginImpl::MetadataCredentialsPluginImpl(std::string key, std::string value)
  : key(std::move(key)), value(std::move(value)) {}

auto MetadataCredentialsPluginImpl::GetMetadata(string_ref /*service_url*/,
                                                string_ref /*method_name*/,
                                                const AuthContext & /*channel_auth_context*/,
                                                std::multimap<grpc::string, grpc::string> *metadata)
  -> Status {

  metadata->insert(std::make_pair(this->key, this->value));
  return Status::OK;
}

PlainPasswordAuthenticator::PlainPasswordAuthenticator(const std::string &username,
                                                       const std::string &password) {
  this->basic = "Basic " + base64_encode(username + ":" + password);
}

auto PlainPasswordAuthenticator::GetCallCredentials() const
  -> std::shared_ptr<grpc::CallCredentials> {
  auto call_creds =
    grpc::MetadataCredentialsFromPlugin(std::unique_ptr<grpc::MetadataCredentialsPlugin>(
      new MetadataCredentialsPluginImpl("authentication", this->basic)));
  return call_creds;
}

} // namespace linkahead::authentication
