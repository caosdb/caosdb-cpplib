/*
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2022 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2022 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
#include "linkahead/acm/user.h"
#include "linkahead/acm/user_impl.h"     // for UserImpl
#include "caosdb/acm/v1alpha1/main.pb.h" // for ProtoUser
#include "linkahead/protobuf_helper.h"   // for ProtoMessageWrapper
#include <utility>                       // for move

namespace linkahead::acm {
using linkahead::utility::ScalarProtoMessageWrapper;
using ProtoUser = caosdb::acm::v1alpha1::User;

UserImpl::UserImpl() = default;

UserImpl::UserImpl(std::string realm, std::string name) {
  if (!name.empty()) {
    this->wrapped->set_name(name);
  }
  if (!realm.empty()) {
    this->wrapped->set_realm(realm);
  }
}

UserImpl::UserImpl(ProtoUser *user) : ScalarProtoMessageWrapper<ProtoUser>(user) {}

UserImpl::UserImpl(std::string name) : UserImpl("", std::move(name)) {}

User::User() : wrapped(std::make_unique<UserImpl>()) {}

User::User(std::unique_ptr<UserImpl> wrapped) : wrapped(std::move(wrapped)) {}

User::User(std::string realm, std::string name)
  : wrapped(std::make_unique<UserImpl>(std::move(realm), std::move(name))) {}

User::User(std::string name) : User({""}, std::move(name)) {}

User::User(const User &user) : User() {
  this->wrapped->wrapped->CopyFrom(*(user.wrapped->wrapped));
}

User::User(User &&user) noexcept : wrapped(std::move(user.wrapped)) {
  user.wrapped = std::make_unique<UserImpl>();
}

auto User::operator=(const User &user) -> User & {
  if (this == &user) {
    return *this;
  }
  this->wrapped->wrapped->CopyFrom(*(user.wrapped->wrapped));

  return *this;
}

auto User::operator=(User &&user) noexcept -> User & {
  if (this == &user) {
    return *this;
  }
  this->wrapped = std::move(user.wrapped);
  user.wrapped = std::make_unique<UserImpl>();

  return *this;
}

User::~User() = default;

auto User::GetName() const -> const std::string & { return this->wrapped->wrapped->name(); }

auto User::SetName(const std::string &name) -> void {
  if (!name.empty()) {
    this->wrapped->wrapped->set_name(name);
  } else {
    this->wrapped->wrapped->clear_name();
  }
}

auto User::GetRealm() const -> const std::string & { return this->wrapped->wrapped->realm(); }

auto User::SetRealm(const std::string &realm) -> void {
  if (!realm.empty()) {
    this->wrapped->wrapped->set_realm(realm);
  } else {
    this->wrapped->wrapped->clear_realm();
  }
}

auto User::GetPassword() const -> const std::string & { return this->wrapped->password; }

auto User::SetPassword(const std::string &password) -> void {
  if (!password.empty()) {
    this->wrapped->password = password;
  } else {
    this->wrapped->password.clear();
  }
}

auto User::ToString() const -> std::string { return this->wrapped->ToString(); }

} // namespace linkahead::acm
