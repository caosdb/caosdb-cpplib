/*
 * This file is a part of the LinkAhead Project.
 * Copyright (C) 2021-2022 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021-2022 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
#include "linkahead/result_set.h" // for ResultSet
#include <memory>                 // for unique_ptr
#include <utility>                // for move, pair
#include <vector>

namespace linkahead::transaction {

ResultSet::iterator::iterator(const ResultSet *result_set_param, int index)
  : current_index(index), result_set(result_set_param) {}

auto ResultSet::iterator::operator*() const -> const Entity & {
  return this->result_set->at(current_index);
}

auto ResultSet::iterator::operator++() -> ResultSet::iterator & {
  current_index++;
  return *this;
}

auto ResultSet::iterator::operator++(int) -> ResultSet::iterator {
  iterator tmp(*this);
  operator++();
  return tmp;
}

auto ResultSet::iterator::operator!=(const iterator &rhs) const -> bool {
  return this->current_index != rhs.current_index;
}

auto ResultSet::begin() const -> ResultSet::iterator { return ResultSet::iterator(this, 0); }

auto ResultSet::end() const -> ResultSet::iterator { return ResultSet::iterator(this, size()); }

MultiResultSet::MultiResultSet(std::vector<std::unique_ptr<Entity>> result_set)
  : AbstractMultiResultSet(std::move(result_set)) {}

} // namespace linkahead::transaction
