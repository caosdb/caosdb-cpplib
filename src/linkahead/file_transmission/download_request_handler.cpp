/*
 * This file is a part of the LinkAhead Project.
 * Copyright (C) 2021-2022 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021-2022 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 *********************************************************************************
 *
 * This is derived work which is heavily based on
 * https://github.com/NeiRo21/grpcpp-bidi-streaming, Commit
 * cd9cb78e5d6d72806c2ec4c703e5e856b223dc96, Aug 10, 2020
 *
 * The orginal work is licensed as
 *
 * > MIT License
 * >
 * > Copyright (c) 2019 NeiRo21
 * >
 * > Permission is hereby granted, free of charge, to any person obtaining a
 * > copy of this software and associated documentation files (the "Software"),
 * > to deal in the Software without restriction, including without limitation
 * > the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * > and/or sell copies of the Software, and to permit persons to whom the
 * > Software is furnished to do so, subject to the following conditions:
 * >
 * > The above copyright notice and this permission notice shall be included in
 * > all copies or substantial portions of the Software.
 * >
 * > THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * > IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * > FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * > AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * > LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * > FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * > DEALINGS IN THE SOFTWARE.
 */
#include "linkahead/file_transmission/download_request_handler.h"
#include <google/protobuf/arena.h>        // for Arena
#include <grpcpp/client_context.h>        // for ClientContext
#include <grpcpp/completion_queue.h>      // for CompletionQueue
#include <grpcpp/support/async_stream.h>  // for ClientAsyncReader
#include <grpcpp/support/status.h>        // for Status, StatusCode
#include <exception>                      // for exception
#include <stdexcept>                      // for runtime_error
#include <string>                         // for basic_string, operator+
#include <utility>                        // for move
#include "linkahead/logging.h"            // for LoggerOutputStream, LINKAH...
#include "linkahead/protobuf_helper.h"    // for get_arena
#include "linkahead/status_code.h"        // for get_status_description
#include "linkahead/transaction_status.h" // for TransactionStatus
// IWYU pragma: no_include <bits/exception.h>
//
namespace linkahead::transaction {
using google::protobuf::Arena;
using linkahead::StatusCode;
using linkahead::utility::get_arena;

DownloadRequestHandler::DownloadRequestHandler(HandlerTag tag, FileTransmissionService::Stub *stub,
                                               grpc::CompletionQueue *cq,
                                               FileDescriptor file_descriptor)
  : tag_(tag), stub_(stub), cq_(cq),
    request_(Arena::CreateMessage<FileDownloadRequest>(get_arena())),
    response_(Arena::CreateMessage<FileDownloadResponse>(get_arena())), state_(CallState::NewCall),
    file_descriptor_(std::move(file_descriptor)), bytesReceived_(0) {}

bool DownloadRequestHandler::OnNext(bool ok) {
  try {
    if (ok) {
      if (state_ == CallState::NewCall) {
        this->handleNewCallState();
      } else if (state_ == CallState::SendingRequest) {
        this->handleSendingRequestState();
      } else if (state_ == CallState::ReceivingFile) {
        this->handleReceivingFileState();
      } else if (state_ == CallState::CallComplete) {
        this->handleCallCompleteState();
        return false;
      }
    } else {
      state_ = CallState::CallComplete;
      rpc_->Finish(&status_, tag_);
    }

    return true;
  } catch (std::exception &e) {
    LINKAHEAD_LOG_ERROR(logger_name) << "DownloadRequestHandler caught an exception: " << e.what();
    transaction_status = TransactionStatus::GENERIC_ERROR(e.what());
    state_ = CallState::CallComplete;
  } catch (...) {
    LINKAHEAD_LOG_ERROR(logger_name) << "Transaction error: unknown exception caught";
    transaction_status =
      TransactionStatus::GENERIC_ERROR("DownloadRequestHandler caught an unknown exception");
    state_ = CallState::CallComplete;
  }

  if (state_ == CallState::NewCall) {
    return false;
  }

  ctx_.TryCancel();

  return true;
}

void DownloadRequestHandler::Cancel() {
  state_ = CallState::CallComplete;
  ctx_.TryCancel();
}

void DownloadRequestHandler::handleNewCallState() {
  LINKAHEAD_LOG_TRACE(logger_name)
    << "Enter DownloadRequestHandler::handleNewCallState. local_path = "
    << file_descriptor_.local_path << ", download_id = " << file_descriptor_.file_transmission_id;
  fileWriter_ = std::make_unique<FileWriter>(file_descriptor_.local_path);

  auto *tid = request_->mutable_file_transmission_id();
  tid->CopyFrom(*(file_descriptor_.file_transmission_id));

  rpc_ = stub_->PrepareAsyncFileDownload(&ctx_, *request_, cq_);

  transaction_status = TransactionStatus::EXECUTING();
  state_ = CallState::SendingRequest;
  rpc_->StartCall(tag_);
  LINKAHEAD_LOG_TRACE(logger_name) << "Leave DownloadRequestHandler::handleNewCallState";
}

void DownloadRequestHandler::handleSendingRequestState() {
  LINKAHEAD_LOG_TRACE(logger_name) << "Enter DownloadRequestHandler::handleSendingRequestState";
  state_ = CallState::ReceivingFile;
  rpc_->Read(response_, tag_);
  LINKAHEAD_LOG_TRACE(logger_name) << "Leave DownloadRequestHandler::handleSendingRequestState";
}

void DownloadRequestHandler::handleReceivingFileState() {
  LINKAHEAD_LOG_TRACE(logger_name) << "Enter DownloadRequestHandler::handleReceivingFileState";
  if (response_->has_chunk()) {
    const auto &chunkData = response_->chunk().data();
    if (chunkData.empty()) {
      LINKAHEAD_LOG_DEBUG(logger_name) << "Received an empty FileChunk, ignoring";
    } else {
      fileWriter_->write(chunkData);
      bytesReceived_ += chunkData.size();
    }

    state_ = CallState::ReceivingFile;
    response_->Clear();
    rpc_->Read(response_, tag_);
  } else {
    throw std::runtime_error("File chunk expected");
  }
  LINKAHEAD_LOG_TRACE(logger_name) << "Leave DownloadRequestHandler::handleReceivingFileState";
}

void DownloadRequestHandler::handleCallCompleteState() {
  LINKAHEAD_LOG_TRACE(logger_name) << "Enter DownloadRequestHandler::handleCallCompleteState";

  switch (status_.error_code()) {
  case grpc::OK: {
    LINKAHEAD_LOG_INFO(logger_name)
      << "DownloadRequestHandler finished successfully (" << file_descriptor_.local_path
      << "): Download complete, " << bytesReceived_ << " bytes received.";
  } break;
  default: {
    auto code(static_cast<StatusCode>(status_.error_code()));
    std::string description(get_status_description(code) +
                            " Original message: " + status_.error_message());
    transaction_status = TransactionStatus(code, description);
    LINKAHEAD_LOG_ERROR(logger_name)
      << "DownloadRequestHandler finished with an error (" << file_descriptor_.local_path
      << "): Download aborted with code " << code << " - " << description;
  } break;
  }

  LINKAHEAD_LOG_TRACE(logger_name) << "Leave DownloadRequestHandler::handleCallCompleteState";
}

} // namespace linkahead::transaction
