#include "linkahead/transaction_handler.h"
#include <string>              // for basic_string
#include "linkahead/logging.h" // for LINKAHEAD_LOG_TRACE_ENTER_AND_LEAVE
// IWYU pragma: no_include <bits/exception.h>

namespace linkahead::transaction {

EntityTransactionHandler::EntityTransactionHandler(HandlerTag tag,
                                                   EntityTransactionService::Stub *stub,
                                                   grpc::CompletionQueue *completion_queue,
                                                   MultiTransactionRequest *request,
                                                   MultiTransactionResponse *response)
  : UnaryRpcHandler(completion_queue), tag_(tag), stub_(stub), request_(request),
    response_(response) {}

void EntityTransactionHandler::handleNewCallState() {
  LINKAHEAD_LOG_TRACE_ENTER_AND_LEAVE(logger_name, "EntityTransactionHandler::handleNewCallState()")

  rpc_ = stub_->PrepareAsyncMultiTransaction(&call_context, *request_, completion_queue);

  state_ = CallState::CallComplete;
  rpc_->StartCall();
  rpc_->Finish(response_, &status_, tag_);
}

} // namespace linkahead::transaction
