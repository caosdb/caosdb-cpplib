/*
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2021 Daniel Hornung <d.hornung@indiscale.com>
 * Copyright (C) 2021-2024 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
#include <gtest/gtest.h>
#include <memory>                         // for unique_ptr
#include <string>                         // for basic_string
#include "linkahead/configuration.h"      // for InsecureConnectionConfigur...
#include "linkahead/connection.h"         // for Connection
#include "linkahead/result_set.h"         // for ResultSet
#include "linkahead/status_code.h"        // for StatusCode
#include "linkahead/transaction.h"        // for Transaction
#include "linkahead/transaction_status.h" // for StatusCode

namespace linkahead::transaction {
using linkahead::configuration::InsecureConnectionConfiguration;
using linkahead::connection::Connection;

TEST(test_issues, test_issue_11) {
  const auto *host = "localhost";
  auto configuration = InsecureConnectionConfiguration(host, 8000);
  Connection connection(configuration);
  auto transaction = connection.CreateTransaction();

  EXPECT_EQ(transaction->GetResultSet().size(), 0);
  transaction->RetrieveById("100");
  EXPECT_EQ(StatusCode::EXECUTING, transaction->ExecuteAsynchronously());
  // Trying to obtain ResultSet while it is still empty.
  EXPECT_EQ(transaction->GetResultSet().size(), 0);
  transaction->WaitForIt();
  EXPECT_EQ(transaction->GetResultSet().size(), 0);
}

} // namespace linkahead::transaction
