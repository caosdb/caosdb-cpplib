/*
 *
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021-2024 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
#include <google/protobuf/arena.h> // for Arena
#include <gtest/gtest.h>
#include "caosdb/entity/v1/main.pb.h" // for Entity, Message, Version, Data...
#include "linkahead/data_type.h"      // for DataType, ReferenceDataType
#include "linkahead/entity.h"         // for Entity

namespace linkahead {
using ProtoEntity = caosdb::entity::v1::Entity;
using caosdb::entity::v1::Message;
using google::protobuf::Arena;
using linkahead::entity::Entity;

TEST(test_protobuf, test_swap_trivial) {
  Arena arena;
  auto *message_source = Arena::CreateMessage<Message>(&arena);
  message_source->set_code(1234);
  message_source->set_description("desc");

  auto *message_destination = Arena::CreateMessage<Message>(&arena);

  EXPECT_EQ(message_source->code(), 1234);
  EXPECT_EQ(message_source->description(), "desc");
  EXPECT_EQ(message_destination->code(), 0);
  EXPECT_EQ(message_destination->description(), "");

  message_source->Swap(message_destination);

  EXPECT_EQ(message_source->code(), 0);
  EXPECT_EQ(message_source->description(), "");
  EXPECT_EQ(message_destination->code(), 1234);
  EXPECT_EQ(message_destination->description(), "desc");
}

TEST(test_protobuf, test_swap_nested) {
  Arena arena;
  auto *entity_source = Arena::CreateMessage<ProtoEntity>(&arena);
  entity_source->set_id("entity_id");
  auto *version_source = entity_source->mutable_version();
  version_source->set_id("version_id");

  auto *entity_destination = Arena::CreateMessage<ProtoEntity>(&arena);
  auto *version_destination = entity_destination->mutable_version();

  EXPECT_EQ(entity_source->id(), "entity_id");
  EXPECT_EQ(entity_source->version().id(), "version_id");
  EXPECT_EQ(version_source->id(), "version_id");
  EXPECT_EQ(entity_destination->id(), "");
  EXPECT_EQ(entity_destination->version().id(), "");
  EXPECT_EQ(version_destination->id(), "");

  entity_source->Swap(entity_destination);

  EXPECT_EQ(entity_source->id(), "");
  EXPECT_EQ(entity_source->version().id(), "");
  EXPECT_EQ(entity_destination->id(), "entity_id");
  EXPECT_EQ(entity_destination->version().id(), "version_id");

  // has not been swapped!
  EXPECT_EQ(version_source->id(), "version_id");
  EXPECT_EQ(version_destination->id(), "");

  // Member pointers to nested messages have been swapped
  EXPECT_EQ(entity_source->mutable_version(), version_destination);
  EXPECT_EQ(entity_destination->mutable_version(), version_source);
}

TEST(test_protobuf, test_copy_nested) {
  Arena arena;
  auto *entity_source = Arena::CreateMessage<ProtoEntity>(&arena);
  auto *data_type_source = entity_source->mutable_data_type();
  data_type_source->mutable_reference_data_type()->set_name("src_per");

  auto *entity_destination = Arena::CreateMessage<ProtoEntity>(&arena);
  auto *data_type_destination = entity_destination->mutable_data_type();
  data_type_destination->mutable_reference_data_type()->set_name("dest_per");

  EXPECT_EQ(entity_source->data_type().reference_data_type().name(), "src_per");
  EXPECT_EQ(entity_destination->data_type().reference_data_type().name(), "dest_per");

  entity_destination->CopyFrom(*entity_source);

  EXPECT_EQ(entity_source->data_type().reference_data_type().name(), "src_per");
  EXPECT_EQ(entity_destination->data_type().reference_data_type().name(), "src_per");

  Entity entity(entity_destination);
  EXPECT_EQ(entity.GetDataType().GetAsReference().GetName(), "src_per");

  const Entity &copy_entity(entity);
  EXPECT_EQ(copy_entity.GetDataType().GetAsReference().GetName(), "src_per");
}

} // namespace linkahead
