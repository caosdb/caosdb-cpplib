/*
 *
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021-2024 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
#include <gtest/gtest.h>               // for TEST, EXPECT_EQ, EXPECT_FALSE, ...
#include <gtest/gtest-message.h>       // for Message
#include <gtest/gtest-test-part.h>     // for TestPartResult
#include <gtest/gtest_pred_impl.h>     // for AssertionResult, Test, EXPECT_EQ
#include <map>                         // for _Rb_tree_const_iterator, map
#include <string>                      // for allocator, basic_string, char...
#include <utility>                     // for pair, move
#include "caosdb/entity/v1/main.pb.h"  // for AtomicDataType, DataType, Entity
#include "linkahead/data_type.h"       // for DataType, ListDataType, Refer...
#include "linkahead/entity.h"          // for Entity, Role
#include "linkahead/logging.h"         // for LINKAHEAD_LOG_DEBUG
#include "linkahead/protobuf_helper.h" // for reset_arena, LINKAHEAD_DEBUG_...

namespace linkahead::entity {
using ProtoEntity = caosdb::entity::v1::Entity;
using ProtoParent = caosdb::entity::v1::Parent;
using ProtoDataType = caosdb::entity::v1::DataType;
using ProtoAtomicDataType = caosdb::entity::v1::AtomicDataType;

TEST(test_data_type, test_atomic) {
  ProtoDataType proto_data_type;

  for (const auto &map_el : atomicdatatype_names) {
    Entity entity;
    entity.SetRole(Role::PROPERTY);
    // the different AtomicDataType are associated with integers
    entity.SetDataType(map_el.first);
    EXPECT_TRUE(entity.GetDataType().IsAtomic());
    EXPECT_EQ(entity.GetDataType().GetAsAtomic(), map_el.first);

    proto_data_type.set_atomic_data_type(static_cast<ProtoAtomicDataType>(map_el.first));
    DataType data_type(&proto_data_type);
    entity.SetDataType(data_type);

    EXPECT_FALSE(data_type.IsReference());
    EXPECT_EQ(data_type.GetAsReference().GetName(), std::basic_string<char>(""));
    EXPECT_FALSE(data_type.IsList());
    EXPECT_TRUE(data_type.IsAtomic());
    EXPECT_EQ(data_type.GetAsAtomic(), map_el.first);
  }
  linkahead::utility::reset_arena();
}

TEST(test_data_type, test_reference) {
  ProtoDataType proto_data_type;

  Entity entity;
  entity.SetRole(Role::PROPERTY);
  entity.SetDataType("Person");
  EXPECT_TRUE(entity.GetDataType().IsReference());
  EXPECT_EQ(entity.GetDataType().GetAsReference().GetName(), "Person");

  proto_data_type.mutable_reference_data_type()->set_name("Person");
  DataType data_type(&proto_data_type);
  entity.SetDataType(data_type);

  EXPECT_TRUE(data_type.IsReference());
  EXPECT_FALSE(data_type.IsList());
  EXPECT_FALSE(data_type.IsAtomic());
  EXPECT_EQ(data_type.GetAsReference().GetName(), "Person");
}

TEST(test_data_type, test_list_of_atomic) {
  for (const auto &map_el : atomicdatatype_names) {
    DataType data_type(map_el.first, true);

    EXPECT_FALSE(data_type.IsReference());
    EXPECT_FALSE(data_type.IsAtomic());
    EXPECT_TRUE(data_type.IsList());
    const auto &list_data_type = data_type.GetAsList();
    EXPECT_EQ(list_data_type.GetReferenceDataType().GetName(), std::basic_string<char>(""));
    EXPECT_TRUE(list_data_type.IsListOfAtomic());
    EXPECT_FALSE(list_data_type.IsListOfReference());
    EXPECT_EQ(list_data_type.GetAtomicDataType(), map_el.first);
  }
}

TEST(test_data_type, test_list_of_reference) {
  auto data_type = DataType("person", true);

  EXPECT_FALSE(data_type.IsReference());
  EXPECT_FALSE(data_type.IsAtomic());
  EXPECT_TRUE(data_type.IsList());

  const auto &list_data_type = data_type.GetAsList();
  EXPECT_TRUE(list_data_type.IsListOfReference());
  EXPECT_FALSE(list_data_type.IsListOfAtomic());
  const auto *wrapped = list_data_type.GetReferenceDataType().GetWrapped();
  LINKAHEAD_DEBUG_MESSAGE_STRING(*wrapped, out)
  LINKAHEAD_LOG_DEBUG("linkahead::entity") << "wrapped " + out;
  EXPECT_EQ(list_data_type.GetReferenceDataType().GetName(), "person");
}

TEST(test_data_type, test_data_type_to_string) {
  DataType data_type1;
  EXPECT_EQ(data_type1.ToString(), "{}\n");

  ProtoDataType proto_data_type;
  DataType data_type2(&proto_data_type);
  EXPECT_EQ(data_type2.ToString(), "{}\n");

  DataType data_type3(AtomicDataType::INTEGER);
  EXPECT_EQ(data_type3.ToString(), "{\n \"atomicDataType\": \"ATOMIC_DATA_TYPE_INTEGER\"\n}\n");
}

TEST(test_data_type, data_type_copy_constructor) {
  DataType data_type("person", true);
  const auto dt_string = data_type.ToString();

  // copy
  const DataType copy_data_type(data_type); // NOLINT (explicit copy)
  EXPECT_EQ(copy_data_type, data_type);
  EXPECT_EQ(dt_string, copy_data_type.ToString());
}

TEST(test_data_type, data_type_move_constructor) {
  DataType data_type("person", true);
  const auto dt_string = data_type.ToString();

  // copy for testing
  const DataType copy_data_type(data_type);
  // move
  const DataType move_data_type(std::move(data_type)); // NOLINT
  EXPECT_NE(data_type, copy_data_type);                // NOLINT
  EXPECT_NE(data_type.ToString(), dt_string);          // NOLINT

  EXPECT_EQ(copy_data_type, move_data_type);
  EXPECT_EQ(move_data_type.ToString(), dt_string);
}

TEST(test_data_type, data_type_copy_assignment) {
  DataType data_type("person", true);
  const auto dt_string = data_type.ToString();

  // copy
  DataType copy_data_type = data_type; // NOLINT (explicit copy)
  EXPECT_EQ(copy_data_type, data_type);
  EXPECT_EQ(dt_string, copy_data_type.ToString());
}

TEST(test_data_type, data_type_move_assignment) {
  DataType data_type("person", true);
  const auto dt_string = data_type.ToString();

  // copy for testing
  const DataType copy_data_type(data_type);
  // move
  DataType move_data_type = std::move(data_type); // NOLINT
  EXPECT_NE(data_type, copy_data_type);           // NOLINT
  EXPECT_NE(data_type.ToString(), dt_string);     // NOLINT

  EXPECT_EQ(copy_data_type, move_data_type);
  EXPECT_EQ(move_data_type.ToString(), dt_string);
}

} // namespace linkahead::entity
