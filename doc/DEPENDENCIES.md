# Dependencies #

Exact versions of some packages here are listed in `requirements.txt`.

## Server ##

```
>=caosdb-server-0.9.0
```

## Compiling ##

```
>=conan-2.5.0 (e.g. with `pip install conan`)
>=cmake-3.13
>=gcc-10.2.0 | >=clang-11
```

## Optional ##

* For checking the schema of a json configuration file: `>=jsonschema-3.2.0`

## Build docs ##

```
doxygen
>=python-3.8
>=pip-21.0.1
python packages from the `doc/requirements.txt` file
```

## Build and run tests ##

```
clang-tidy-16
>=gtest-1.10.0
```

## Coverage ##

```
>=gcc-10.2.0
>=lcov-1.16
```

## Formatting ##

```
>=clang-format-16
```
