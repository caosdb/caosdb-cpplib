# Features

This library provides a client for communication with a LinkAhead server over the
LinkAhead GRPC-APIs. The client supports

* Transaction of entities and file transmission.
* Authentication via Username/Password.
* Secure communication via TLS.
* Multiple connections with different servers.
* Logging.
* File-based configuration.

The core is written in C++ and makes heavy use of object-oriented patterns. It
results in a c++ library -- liblinkahead.

For using the library in a plain C context, a thin wrapper is included
-- libclinkahead. It wrapps the C++ classes into structs and the member methods
into functions which operate on the wrapped C++ instances.

Minimal testing executables in C++ and C are included as well, but they only
serve as just that -- minimal testing.

## LinkAhead Entitiy API

This library implements a subset of the LinkAhead Entitiy API (caosdb.entity.v1)
as a client.

Current limitations: The implementation does not support mixing retrievals with
write-transactions.

### Query

Support for `FIND`, `SELECT` and `COUNT` queries.

## LinkAhead Info API

This library implements the LinkAhead Info API (caosdb.info.v1) as a client
without limitations.

## LinkAhead ACM (Access Control Management) API

This library implements small parts of the experimental ACM API
(caosdb.acm.v1alpha1) as a client.

## Thread-safety and Concurrency

Currently, the library should not be considered thread-safe.

The `linkahead::transaction::Transaction` class is designed with an with an
interface ready for concurrent and asyncronous transactions. While it is
possible to execute transactions in non-blocking mode and call `GetStatus`
concurrently, it is not possible to use any of the non-const methods of the
`Transaction` class in a thread-safe way.
