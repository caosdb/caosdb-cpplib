/*
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021-2024 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef LINKAHEAD_CERTIFICATE_PROVIDER_H
#define LINKAHEAD_CERTIFICATE_PROVIDER_H

#include <filesystem> // for path
namespace linkahead::configuration {
using std::filesystem::path;

class CertificateProvider {
public:
  [[nodiscard]] auto virtual GetCertificatePem() const -> std::string = 0;
  virtual ~CertificateProvider() = default;
};

class PemFileCertificateProvider : public CertificateProvider {
private:
  std::string certificate_provider;

public:
  explicit PemFileCertificateProvider(const path &path);
  [[nodiscard]] auto GetCertificatePem() const -> std::string override;
};

class PemCertificateProvider : public CertificateProvider {
private:
  std::string certificate_provider;

public:
  explicit PemCertificateProvider(std::string certificate_provider);
  [[nodiscard]] auto GetCertificatePem() const -> std::string override;
};
} // namespace linkahead::configuration
#endif
