/*
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021-2024 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef LINKAHEAD_TRANSACTION_STATUS_H
#define LINKAHEAD_TRANSACTION_STATUS_H

#include "linkahead/status_code.h"
#include "linkahead/exceptions.h"
#include <memory> // for shared_ptr, unique_ptr
#include <string> // for string

namespace linkahead::transaction {
using linkahead::StatusCode;
using linkahead::exceptions::AuthenticationError;
using linkahead::exceptions::ConnectionError;
using linkahead::exceptions::Exception;
using linkahead::exceptions::TransactionError;
using linkahead::exceptions::TransactionStatusError;
using linkahead::exceptions::TransactionTypeError;

/**
 * Define static factory method in the TransactionStatus class.
 */
#define LINKAHEAD_TRANSACTION_STATUS_DEFAULT_FACTORY(_StatusName, _StatusCode)                     \
  inline static auto _StatusName()->const TransactionStatus & {                                    \
    static const TransactionStatus instance(_StatusCode,                                           \
                                            linkahead::get_status_description(_StatusCode));       \
    return instance;                                                                               \
  }

/**
 * TransactionStatus indicates the current status of a transaction and, when it
 * has already terminated, whether the transaction has been successful or not.
 *
 * A status code of 0 denotes a generic success state, positive values indicate
 * errors, and negative values indicate other states, such as different stages
 * of a transaction in process.
 */
class TransactionStatus {
public:
  /**
   * Factory for an INITIAL status.
   *
   * This status means that the transaction has not been executed yet and the
   * transaction does not contain any sub-transactions yet.
   */
  LINKAHEAD_TRANSACTION_STATUS_DEFAULT_FACTORY(INITIAL, StatusCode::INITIAL)
  /**
   * Factory for a GO_ON status.
   *
   * This status means that the transaction has not been executed yet but it
   * already contains sub-transaction and more subtransactions may be added.
   *
   * However, it also can be executed right now.
   */
  LINKAHEAD_TRANSACTION_STATUS_DEFAULT_FACTORY(GO_ON, StatusCode::GO_ON)
  /**
   * Factory for a READY status.
   *
   * This status means that the transaction has not been executed yet but it is
   * ready to be executed and it is not possible anymore to add further
   * sub-transactions.
   */
  LINKAHEAD_TRANSACTION_STATUS_DEFAULT_FACTORY(READY, StatusCode::READY)
  /**
   * Factory for an EXECUTING status.
   *
   * This status means that the transaction is currently being executed.
   */
  LINKAHEAD_TRANSACTION_STATUS_DEFAULT_FACTORY(EXECUTING, StatusCode::EXECUTING)
  /**
   * Factory for a SUCCESS status.
   *
   * This status means that the transaction has been executed successfully.
   */
  LINKAHEAD_TRANSACTION_STATUS_DEFAULT_FACTORY(SUCCESS, StatusCode::SUCCESS)
  /**
   * Factory for a CANCELLED status.
   *
   * This status means that the transaction has been canceled and should not be
   * used anymore.
   */
  LINKAHEAD_TRANSACTION_STATUS_DEFAULT_FACTORY(CANCELLED, StatusCode::CANCELLED)
  /**
   * Factory for a CONNECTION_ERROR status.
   *
   * This status means that the connection could not be established. This is
   * possibly due to misconfiguration of the client, errors in the network or
   * because the server is down.
   */
  LINKAHEAD_TRANSACTION_STATUS_DEFAULT_FACTORY(CONNECTION_ERROR, StatusCode::CONNECTION_ERROR)
  /**
   * Factory for an AUTHENTICATION_ERROR status.
   *
   * This status means that the RPC layer reported an authentication error.
   */
  LINKAHEAD_TRANSACTION_STATUS_DEFAULT_FACTORY(AUTHENTICATION_ERROR,
                                               StatusCode::AUTHENTICATION_ERROR)
  /**
   * Another factory for an TRANSACTION_ERROR Status with a detailed
   * description.
   */
  inline static auto AUTHENTICATION_ERROR(const std::string &details) -> const TransactionStatus {
    return TransactionStatus(StatusCode::AUTHENTICATION_ERROR,
                             linkahead::get_status_description(StatusCode::AUTHENTICATION_ERROR) +
                               " Original error: " + details);
  }
  /**
   * Factory for a FILE_UPLOAD_ERROR status.
   *
   * This status means that the transaction failed during the upload of the
   * file blobs of file entities.
   */
  LINKAHEAD_TRANSACTION_STATUS_DEFAULT_FACTORY(FILE_UPLOAD_ERROR, StatusCode::FILE_UPLOAD_ERROR);
  /**
   * Factory for a FILE_DOWN_ERROR status.
   *
   * This status means that the transaction failed during the download of the
   * file blobs of file entities.
   */
  LINKAHEAD_TRANSACTION_STATUS_DEFAULT_FACTORY(FILE_DOWNLOAD_ERROR,
                                               StatusCode::FILE_DOWNLOAD_ERROR);
  /**
   * Factory for a TRANSACTION_ERROR status.
   *
   * This status means that the transaction failed due to errors thrown by the
   * server.
   */
  LINKAHEAD_TRANSACTION_STATUS_DEFAULT_FACTORY(TRANSACTION_ERROR,
                                               StatusCode::GENERIC_TRANSACTION_ERROR)
  /**
   * Factory for a SPOILED status.
   *
   * This status means that the transaction's result set has been released and
   * GetResultSet() will not return the actual results of the transaction
   * anymore.
   */
  LINKAHEAD_TRANSACTION_STATUS_DEFAULT_FACTORY(SPOILED, StatusCode::SPOILED);
  /**
   * Another factory for a TRANSACTION_ERROR status with a detailed
   * description.
   */
  inline static auto TRANSACTION_ERROR(const std::string &details) -> const TransactionStatus {
    return TransactionStatus(
      StatusCode::GENERIC_TRANSACTION_ERROR,
      linkahead::get_status_description(StatusCode::GENERIC_TRANSACTION_ERROR) +
        " Original error: " + details);
  }
  /**
   * Factory for a RPC_ERROR with a detailed description.
   *
   * This status is used for any error on the RPC layer.
   */
  inline static auto RPC_ERROR(const std::string &details) -> const TransactionStatus {
    // We use the GENERIC_RPC_ERROR here because we might want to add further
    // RPC_ERROR states with different error codes (which stem from GRPC) here
    // in the future.
    return TransactionStatus(StatusCode::GENERIC_RPC_ERROR,
                             linkahead::get_status_description(StatusCode::GENERIC_RPC_ERROR) +
                               " Original error: " + details);
  }

  /**
   * Factory for a GENERIC_ERROR status.
   *
   * This status means that the transaction failed due to errors which
   * supposedly do not have a special handling.
   */
  inline static auto GENERIC_ERROR(const std::string &details) {
    return TransactionStatus(StatusCode::GENERIC_ERROR,
                             linkahead::get_status_description(StatusCode::GENERIC_ERROR) +
                               "Original error: " + details);
  }

  inline auto ThrowExceptionIfError() const -> void {
    TransactionStatus::ThrowExceptionIfError(this->code, this->description);
  }

  inline static auto ThrowExceptionIfError(StatusCode code, const std::string &description)
    -> void {
    if (!IsError(code)) {
      return;
    }
    switch (code) {
    case StatusCode::CONNECTION_ERROR:
      throw ConnectionError(description);
    case StatusCode::AUTHENTICATION_ERROR:
      throw AuthenticationError(description);
    case StatusCode::GENERIC_TRANSACTION_ERROR:
      throw TransactionError(description);
    case StatusCode::TRANSACTION_STATUS_ERROR:
      throw TransactionStatusError(description);
    case StatusCode::TRANSACTION_TYPE_ERROR:
      throw TransactionTypeError(description);
    default:
      throw Exception(code, description);
    }
  }

  /**
   * Return true if the given StatusCode represents an erroneous state.
   */
  inline static auto IsError(StatusCode code) -> bool { return code > 0; };

  /**
   * Return true if this TransactionStatus represents a terminated state.
   */
  inline auto IsTerminated() const -> bool { return this->code >= 0; };

  /**
   * Return true if this TransactionStatus represents an erroneous state.
   */
  inline auto IsError() const -> bool { return TransactionStatus::IsError(this->code); };

  /**
   * Return a description of the erroneous state.
   *
   * No description yields an empty string.
   */
  inline auto GetDescription() const -> const std::string & { return this->description; }

  /**
   * Return the status code of the state.
   */
  inline auto GetCode() const -> StatusCode { return this->code; }

  TransactionStatus(StatusCode code, const std::string &description)
    : code(code), description(description){};

private:
  /**
   * The code is an identifier of errors.
   */
  StatusCode code;

  /**
   * Description of the error
   */
  std::string description;
};

} // namespace linkahead::transaction
#endif
