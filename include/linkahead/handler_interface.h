/*
 * This file is a part of the LinkAhead Project.
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021-2024 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 *********************************************************************************
 *
 * This is derived work which is heavily based on
 * https://github.com/NeiRo21/grpcpp-bidi-streaming, Commit
 * cd9cb78e5d6d72806c2ec4c703e5e856b223dc96, Aug 10, 2020
 *
 * The orginal work is licensed as
 *
 * > MIT License
 * >
 * > Copyright (c) 2019 NeiRo21
 * >
 * > Permission is hereby granted, free of charge, to any person obtaining a
 * > copy of this software and associated documentation files (the "Software"),
 * > to deal in the Software without restriction, including without limitation
 * > the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * > and/or sell copies of the Software, and to permit persons to whom the
 * > Software is furnished to do so, subject to the following conditions:
 * >
 * > The above copyright notice and this permission notice shall be included in
 * > all copies or substantial portions of the Software.
 * >
 * > THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * > IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * > FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * > AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * > LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * > FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * > DEALINGS IN THE SOFTWARE.
 */
#ifndef LINKAHEAD_HANDLER_INTERFACE_H
#define LINKAHEAD_HANDLER_INTERFACE_H

#include "linkahead/transaction_status.h" // for TransactionStatus
#include <memory>
#include <string>

namespace linkahead::transaction {

const static std::string logger_name = "linkahead::transaction";
/*
 * Baseclass for UnaryRpcHandler, DownloadRequestHandler and
 * UploadRequestHandler
 *
 * It handles a request: Its status is contained in the transaction_status
 * member variable and the functions Start, OnNext and Cancel need to be
 * overwritten by child classes.
 */
class HandlerInterface {
public:
  HandlerInterface() : transaction_status(TransactionStatus::READY()) {}

  virtual ~HandlerInterface() = default;

  virtual void Start() = 0;

  /*
   * ok indicates whether the current request is in a good state or not. If
   * ok is false, the request will be ended.
   *
   * returns false if the handler is done
   */
  virtual bool OnNext(bool ok) = 0;

  virtual void Cancel() = 0;

  inline TransactionStatus GetStatus() { return this->transaction_status; }

protected:
  TransactionStatus transaction_status;
};

using HandlerPtr = std::unique_ptr<HandlerInterface>;
using HandlerTag = HandlerPtr *;

} // namespace linkahead::transaction

#endif
