/*
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2022 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2022 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
#ifndef LINKAHEAD_RESULT_SET_H
#define LINKAHEAD_RESULT_SET_H

#include <iosfwd>             // for ptrdiff_t
#include <iterator>           // for output_iterator_tag
#include <memory>             // for unique_ptr, make_unique
#include <utility>            // for move
#include <vector>             // for vector
#include "linkahead/entity.h" // for Entity
// IWYU pragma: no_include <ext/alloc_traits.h>

namespace linkahead::transaction {
using linkahead::entity::Entity;

/**
 * Abstract base class for the results of a Transaction.
 */
class ResultSet {
  class iterator;

public:
  virtual ~ResultSet() = default;
  [[nodiscard]] virtual auto size() const noexcept -> size_t = 0;
  [[nodiscard]] virtual auto at(const int index) const -> const Entity & = 0;
  [[nodiscard]] virtual auto mutable_at(int index) const -> Entity * = 0;
  /**
   * Return the Entity at the given index.
   *
   * This method releases the entity from the underlying collection and thus
   * leaves the ResultSet in a corrupted state.
   *
   * This method can be called only once for each index.
   */
  [[nodiscard]] virtual auto release_at(int index) -> Entity * = 0;
  auto begin() const -> iterator;
  auto end() const -> iterator;

private:
  class iterator {
  public:
    using iterator_category = std::output_iterator_tag;
    using value_type = Entity;
    using difference_type = std::ptrdiff_t;
    using pointer = Entity *;
    using reference = Entity &;

    explicit iterator(const ResultSet *result_set, int index = 0);
    auto operator*() const -> const Entity &;
    auto operator++() -> iterator &;
    auto operator++(int) -> iterator;
    auto operator!=(const iterator &rhs) const -> bool;

  private:
    int current_index = 0;
    const ResultSet *result_set;
  };
};

class AbstractMultiResultSet : public ResultSet {
public:
  /**
   * Copy Constructor.
   *
   * Copies the underlying collection of entities.
   */
  inline AbstractMultiResultSet(const AbstractMultiResultSet &original) {
    for (const Entity &entity : original) {
      this->items.push_back(std::make_unique<Entity>(entity));
    }
  }
  virtual ~AbstractMultiResultSet() = default;
  inline explicit AbstractMultiResultSet(std::vector<std::unique_ptr<Entity>> result_set)
    : items(std::move(result_set)) {}
  [[nodiscard]] inline auto size() const noexcept -> size_t override { return this->items.size(); }
  [[nodiscard]] inline auto at(const int index) const -> const Entity & override {
    return *(this->items.at(index));
  }
  [[nodiscard]] inline auto mutable_at(int index) const -> Entity * override {
    return this->items.at(index).get();
  }
  /**
   * Return the Entity at the given index.
   *
   * This method releases the entity from the underlying collection and thus
   * leaves the ResultSet in a corrupted state.
   *
   * This method can be called only once for each index.
   */
  [[nodiscard]] inline auto release_at(int index) -> Entity * override {
    return this->items.at(index).release();
  }
  /**
   * Remove all entities from this result set.
   */
  inline auto clear() noexcept -> void { this->items.clear(); }

protected:
  std::vector<std::unique_ptr<Entity>> items;
};

/**
 * Container with results of a transaction.
 */
class MultiResultSet : public AbstractMultiResultSet {
public:
  ~MultiResultSet() = default;
  explicit MultiResultSet(std::vector<std::unique_ptr<Entity>> result_set);
};

} // namespace linkahead::transaction
#endif
