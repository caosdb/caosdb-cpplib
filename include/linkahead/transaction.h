/*
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021-2024 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
#ifndef LINKAHEAD_TRANSACTION_H
#define LINKAHEAD_TRANSACTION_H

#include <google/protobuf/util/json_util.h> // for JsonPrintOptions, Messag...
#include <grpcpp/completion_queue.h>        // for CompletionQueue
#include <future>                           // for future
#include <iterator>                         // for next
#include <map>                              // for map
#include <memory>                           // for unique_ptr, shared_ptr
#include <mutex>                            // for mutex
#include <string>                           // for string, basic_string
#include <vector>                           // for vector
#include "caosdb/entity/v1/main.grpc.pb.h"  // for EntityTransactionService
#include "caosdb/entity/v1/main.pb.h"       // for MultiTransactionRequest
#include "linkahead/entity.h"               // for Entity
#include "linkahead/file_descriptor.h"      // for FileDescriptor
#include "linkahead/handler_interface.h"    // for logger_name, HandlerInte...
#include "linkahead/logging.h"              // for LINKAHEAD_LOG_ERROR_AND_...
#include "linkahead/protobuf_helper.h"      // for get_arena
#include "linkahead/result_set.h"           // for MultiResultSet, ResultSet
#include "linkahead/result_table.h"         // for ResultTable
#include "linkahead/status_code.h"          // for StatusCode
#include "linkahead/transaction_status.h"   // for StatusCode, TransactionS...
#include "linkahead/value.h"                // for Value

/**
 * Do all necessary checks and assure that another retrieval (by id or by
 * query) can be added as a sub-request to a transaction.
 */
#define ASSERT_CAN_ADD_RETRIEVAL                                                                   \
  if (this->status.GetCode() != StatusCode::INITIAL &&                                             \
      this->status.GetCode() != StatusCode::GO_ON) {                                               \
    return StatusCode::TRANSACTION_STATUS_ERROR;                                                   \
  }                                                                                                \
  switch (this->transaction_type) {                                                                \
  case TransactionType::NONE:                                                                      \
    this->transaction_type = TransactionType::READ_ONLY;                                           \
    break;                                                                                         \
  case TransactionType::READ_ONLY:                                                                 \
    break;                                                                                         \
  case TransactionType::MIXED_READ_AND_WRITE:                                                      \
    break;                                                                                         \
  default:                                                                                         \
    LINKAHEAD_LOG_ERROR_AND_RETURN_STATUS(                                                         \
      logger_name, StatusCode::TRANSACTION_TYPE_ERROR,                                             \
      "You cannot add a retrieval to this transaction because it has the "                         \
      "wrong TransactionType.")                                                                    \
  }

/**
 * Do all necessary checks and assure that another retrieval (by id or by
 * query) can be added as a sub-request to a transaction.
 */
#define ASSERT_CAN_ADD_QUERY                                                                       \
  ASSERT_CAN_ADD_RETRIEVAL                                                                         \
  if (this->has_query) {                                                                           \
    LINKAHEAD_LOG_ERROR_AND_RETURN_STATUS(                                                         \
      logger_name, StatusCode::UNSUPPORTED_FEATURE,                                                \
      "Currently the number of queries which can be processed in a single "                        \
      "transaction is limitted to one.");                                                          \
  }

/**
 * Do all necessary checks and assure that another deletion can be added as a
 * sub-request to a transaction.
 */
#define ASSERT_CAN_ADD_DELETION                                                                    \
  if (this->status.GetCode() != StatusCode::INITIAL &&                                             \
      this->status.GetCode() != StatusCode::GO_ON) {                                               \
    return StatusCode::TRANSACTION_STATUS_ERROR;                                                   \
  }                                                                                                \
  switch (this->transaction_type) {                                                                \
  case TransactionType::NONE:                                                                      \
    this->transaction_type = TransactionType::MIXED_WRITE;                                         \
  case TransactionType::DELETE_ONLY:                                                               \
  case TransactionType::MIXED_WRITE:                                                               \
  case TransactionType::MIXED_READ_AND_WRITE:                                                      \
    break;                                                                                         \
  default:                                                                                         \
    LINKAHEAD_LOG_ERROR_AND_RETURN_STATUS(                                                         \
      logger_name, StatusCode::TRANSACTION_TYPE_ERROR,                                             \
      "You cannot add a deletion to this transaction because it has the "                          \
      "wrong TransactionType.")                                                                    \
  }

/**
 * Do all necessary checks and assure that another insertion can be added as a
 * sub-request to a transaction.
 */
#define ASSERT_CAN_ADD_INSERTION                                                                   \
  if (this->status.GetCode() != StatusCode::INITIAL &&                                             \
      this->status.GetCode() != StatusCode::GO_ON) {                                               \
    return StatusCode::TRANSACTION_STATUS_ERROR;                                                   \
  }                                                                                                \
  switch (this->transaction_type) {                                                                \
  case TransactionType::NONE:                                                                      \
    this->transaction_type = TransactionType::MIXED_WRITE;                                         \
  case TransactionType::INSERT_ONLY:                                                               \
  case TransactionType::MIXED_WRITE:                                                               \
  case TransactionType::MIXED_READ_AND_WRITE:                                                      \
    break;                                                                                         \
  default:                                                                                         \
    LINKAHEAD_LOG_ERROR_AND_RETURN_STATUS(                                                         \
      logger_name, StatusCode::TRANSACTION_TYPE_ERROR,                                             \
      "You cannot add an insertion to this transaction because it has the "                        \
      "wrong TransactionType.")                                                                    \
  }

/**
 * Do all necessary checks and assure that another update can be added as a
 * sub-request to a transaction.
 */
#define ASSERT_CAN_ADD_UPDATE                                                                      \
  if (this->status.GetCode() != StatusCode::INITIAL &&                                             \
      this->status.GetCode() != StatusCode::GO_ON) {                                               \
    return StatusCode::TRANSACTION_STATUS_ERROR;                                                   \
  }                                                                                                \
  switch (this->transaction_type) {                                                                \
  case TransactionType::NONE:                                                                      \
    this->transaction_type = TransactionType::MIXED_WRITE;                                         \
  case TransactionType::UPDATE_ONLY:                                                               \
  case TransactionType::MIXED_WRITE:                                                               \
  case TransactionType::MIXED_READ_AND_WRITE:                                                      \
    break;                                                                                         \
  default:                                                                                         \
    LINKAHEAD_LOG_ERROR_AND_RETURN_STATUS(                                                         \
      logger_name, StatusCode::TRANSACTION_TYPE_ERROR,                                             \
      "You cannot add an update to this transaction because it has the "                           \
      "wrong TransactionType.")                                                                    \
  }                                                                                                \
  if (!entity->HasId()) {                                                                          \
    LINKAHEAD_LOG_ERROR_AND_RETURN_STATUS(                                                         \
      logger_name, StatusCode::ORIGINAL_ENTITY_MISSING_ID,                                         \
      "You cannot update this entity without any id. Probably you did not "                        \
      "retrieve it first? Entity updates should always start with the "                            \
      "retrieval of the existing entity which may then be changed.")                               \
  }

/**
 * @brief Creation and execution of transactions.
 * @author Timm Fitschen
 * @date 2021-08-05
 */
namespace linkahead::transaction {
using caosdb::entity::v1::EntityResponse;
using caosdb::entity::v1::EntityTransactionService;
using caosdb::entity::v1::FileDownloadRequest;
using caosdb::entity::v1::FileDownloadResponse;
using caosdb::entity::v1::FileTransmissionId;
using caosdb::entity::v1::FileTransmissionService;
using caosdb::entity::v1::FileUploadRequest;
using caosdb::entity::v1::FileUploadResponse;
using caosdb::entity::v1::IdResponse;
using caosdb::entity::v1::MultiTransactionRequest;
using caosdb::entity::v1::MultiTransactionResponse;
using caosdb::entity::v1::RegisterFileUploadRequest;
using caosdb::entity::v1::RegisterFileUploadResponse;
using linkahead::entity::Entity;
using linkahead::entity::FileDescriptor;
using linkahead::entity::Value;
using linkahead::transaction::TransactionStatus;
using RetrieveResponse = caosdb::entity::v1::RetrieveResponse;
using TransactionResponseCase = caosdb::entity::v1::TransactionResponse::TransactionResponseCase;
using google::protobuf::Arena;
using linkahead::utility::get_arena;

class Transaction;

/**
 * @brief Create a transaction via `LinkAheadConnection.createTransaction()`
 */
class Transaction {
public:
  /**
   * The transaction type restricts the kind of sub-transaction which may be
   * added to a transaction (insertion, update, deletion, retrieval).
   *
   * @note MIXED_READ_AND_WRITE and MIXED_WRITE transaction are not supported
   * yet.
   */
  enum TransactionType {
    NONE,                //!< Unspecified or not specified yet.
    READ_ONLY,           //!< Only retrievals (by id, by query)
    INSERT_ONLY,         //!< Only insertions
    UPDATE_ONLY,         //!< Only updates
    DELETE_ONLY,         //!< Only deletions
    MIXED_WRITE,         //!< Only insertions, deletions, updates
    MIXED_READ_AND_WRITE //!< all kind of transaction.
  };
  Transaction(std::shared_ptr<EntityTransactionService::Stub> entity_service,
              std::shared_ptr<FileTransmissionService::Stub> file_service);

  ~Transaction();

  Transaction(const Transaction &) = delete;
  Transaction &operator=(const Transaction &) = delete;
  Transaction(Transaction &&) = delete;
  Transaction &operator=(Transaction &&) = delete;

  /**
   * Add an entity id to this transaction for retrieval and also download the
   * file.
   *
   * If the entity doesn't have a file a warning is appended.
   *
   * If the file cannot be downloaded due to unsufficient permissions an error
   * is appended.
   */
  auto RetrieveAndDownloadFileById(const std::string &id, const std::string &local_path) noexcept
    -> StatusCode;

  /**
   * Add an entity id to this transaction for retrieval.
   *
   * The retrieval is being processed when the Execute() or
   * ExecuteAsynchronously() methods of this transaction are called.
   */
  auto RetrieveById(const std::string &id) noexcept -> StatusCode;

  /**
   * Add all entity ids to this transaction for retrieval.
   */
  template <class InputIterator>
  inline auto RetrieveById(InputIterator begin, InputIterator end) noexcept -> StatusCode;

  /**
   * Add a query to this transaction.
   *
   * Currently, only FIND and COUNT queries are supported.
   */
  auto Query(const std::string &query) noexcept -> StatusCode;

  /**
   * Add the entity to this transaction for an insertion.
   *
   * The insertion is being processed when the Execute() or
   * ExecuteAsynchronously() methods of this transaction are called.
   *
   * Changing the entity afterwards results in undefined behavior.
   */
  auto InsertEntity(Entity *entity) noexcept -> StatusCode;

  /**
   * Add the entity to this transaction for an update.
   *
   * The update is being processed when the Execute() or
   * ExecuteAsynchronously() methods of this transaction are called.
   *
   * Changing the entity afterwards results in undefined behavior.
   */
  auto UpdateEntity(Entity *entity) noexcept -> StatusCode;

  /**
   * Add an entity id to this transaction for deletion.
   *
   * The deletion is being processed when the Execute() or
   * ExecuteAsynchronously() methods of this transaction are called.
   */
  auto DeleteById(const std::string &id) noexcept -> StatusCode;

  /**
   * Execute this transaction in blocking mode and return the status.
   */
  auto Execute() -> TransactionStatus;

  /**
   * Execute this transaction in non-blocking mode and return immediately.
   *
   * A client may request the current status at any time via GetStatus().
   *
   * Use WaitForIt() to join the back-ground execution of this transaction.
   */
  auto ExecuteAsynchronously() noexcept -> StatusCode;

  /**
   * Join the background execution and return the status when the execution
   * terminates.
   *
   * Use this after ExecuteAsynchronously(), otherwise the TransactionStatus still remains
   * EXECUTING.
   */
  auto WaitForIt() const noexcept -> TransactionStatus;

  /**
   * Return the current status of the transaction.
   */
  [[nodiscard]] inline auto GetStatus() const noexcept -> TransactionStatus { return this->status; }

  /**
   * Return the ResultSet of this transaction.
   *
   * Note: If this method is called before the transaction has terminated
   * successfully (as indicated by GetStatus().GetCode == 0) this method has
   * undefined behavior.
   */
  [[nodiscard]] inline auto GetResultSet() const noexcept -> const ResultSet & {
    if (this->GetStatus().GetCode() < 0) {
      LINKAHEAD_LOG_ERROR(logger_name)
        << "GetResultSet was called before the transaction has terminated. This is a programming "
           "error of the code which uses the transaction.";
      // TODO(tf) This is a really bad SegFault factory. When the transaction
      // terminates and the result_set is being overriden, the unique_ptr
      // created here will be deleted and any client of the return ResultSet
      // will have a SegFault.
    } else if (this->GetStatus().GetCode() == StatusCode::SPOILED) {
      LINKAHEAD_LOG_ERROR(logger_name)
        << "GetResultSet was called on a \"spoiled\" transaction. That means "
           "that the result set has already been released via "
           "ReleaseResultSet(). This is a programming error of the code which "
           "uses the transaction.";
    }
    return *(this->result_set.get());
  }

  /**
   * Return the ResultTable of a SELECT query.
   */
  [[nodiscard]] inline auto GetResultTable() const noexcept -> const ResultTable & {
    if (this->GetStatus().GetCode() < 0) {
      LINKAHEAD_LOG_ERROR(logger_name)
        << "GetResultTable was called before the transaction has terminated. This is a programming "
           "error of the code which uses the transaction.";
    } else if (this->GetStatus().GetCode() == StatusCode::SPOILED) {
      LINKAHEAD_LOG_ERROR(logger_name)
        << "GetResultTable was called on a \"spoiled\" transaction. That means "
           "that the result table has already been released via "
           "ReleaseResultTable(). This is a programming error of the code which "
           "uses the transaction.";
    }
    return *(this->result_table.get());
  }

  /**
   * Return the ResultSet of this transaction.
   *
   * This method releases the ResultSet from the transaction leaving it in a
   * currupted state.
   *
   * This method can be called only once and only after the transaction has terminated.
   *
   * Otherwise, this method has undefined behavior.
   */
  [[nodiscard]] inline auto ReleaseResultSet() noexcept -> const ResultSet * {
    this->status = TransactionStatus::SPOILED();
    auto result_set = this->result_set.release();
    this->result_set = std::make_unique<MultiResultSet>(std::vector<std::unique_ptr<Entity>>());
    return result_set;
  }

  /**
   * Return the result of a count query
   *
   * Only meaningful if there was exactly one COUNT query executed in
   * this transaction. In all other cases, the return value will be
   * -1.
   */
  [[nodiscard]] inline auto GetCountResult() const noexcept -> long { return query_count; }

  /**
   * Return the number of sub-requests in this transaction.
   *
   * This is meant for debugging because the number of sub-requests is a
   * GRPC-API detail.
   */
  [[nodiscard]] inline auto GetRequestCount() const -> int {
    return this->request->requests_size();
  }

  /**
   * Get a JSON representation of the response.
   *
   * For debugging.
   */
  inline auto ResponseToString() const -> const std::string {
    google::protobuf::util::JsonPrintOptions options;
    options.add_whitespace = true;
    std::string out;
    google::protobuf::util::MessageToJsonString(*this->response, &out, options);
    return out;
  }

  /**
   * Get a JSON representation of the request.
   *
   * For debugging.
   */
  inline auto RequestToString() const -> const std::string {
    google::protobuf::util::JsonPrintOptions options;
    options.add_whitespace = true;
    std::string out;
    google::protobuf::util::MessageToJsonString(*this->request, &out, options);
    return out;
  }

  /**
   * Return the vector which holds all the files which are to be uploaded.
   */
  inline auto GetUploadFiles() const -> const std::vector<FileDescriptor> & { return upload_files; }

  /**
   * Cancel this transaction after it has been started with
   * ExecuteAsynchronously().
   *
   * This will cancel any active handler and drains the completion_queue.
   */
  auto Cancel() -> void;

protected:
  /**
   * Await and process the current handler's results.
   *
   * This implies consecutive calls to the handler's OnNext function.
   */
  auto ProcessCalls() -> TransactionStatus;

  /**
   * Return the Arena where this transaction may create Message instances.
   *
   * Currently, this implementation is only a call to
   * linkahead::utility::get_arena(), but in the future we might want to have a
   * smarter memory management.
   */
  inline auto GetArena() const -> Arena * { return get_arena(); }

private:
  /**
   * Await and process the termination of this transaction.
   *
   * To be called at the end of DoExecuteTransaction on success.
   */
  auto ProcessTerminated() const noexcept -> void;
  auto ProcessRetrieveResponse(RetrieveResponse *retrieve_response,
                               std::vector<std::unique_ptr<Entity>> *entities,
                               bool *set_error) const noexcept -> std::unique_ptr<Entity>;
  /**
   * This functions actually does all the work. Also, it is the one which is
   * going to be send to the background thread by ExecuteAsynchronously.
   */
  auto DoExecuteTransaction() noexcept -> void;
  mutable std::mutex transaction_mutex;
  mutable std::future<void> transaction_future;
  grpc::CompletionQueue completion_queue;
  std::unique_ptr<HandlerInterface> handler_;

  std::vector<FileDescriptor> upload_files;
  std::map<std::string, FileDescriptor> download_files;

  bool has_query = false;
  TransactionType transaction_type = TransactionType::NONE;
  mutable TransactionStatus status = TransactionStatus::INITIAL();
  std::shared_ptr<EntityTransactionService::Stub> entity_service;
  std::shared_ptr<FileTransmissionService::Stub> file_service;
  MultiTransactionRequest *request;
  mutable MultiTransactionResponse *response;
  std::string error_message;
  mutable long query_count;
  mutable std::unique_ptr<ResultSet> result_set;
  mutable std::unique_ptr<ResultTable> result_table;
};

template <class InputIterator>
inline auto Transaction::RetrieveById(InputIterator begin, InputIterator end) noexcept
  -> StatusCode {
  ASSERT_CAN_ADD_RETRIEVAL

  auto next = begin;
  while (next != end) {
    auto *sub_request = this->request->add_requests();
    sub_request->mutable_retrieve_request()->set_id(*next);
    next = std::next(next);
  }

  this->status = TransactionStatus::GO_ON();
  return this->status.GetCode();
}

} // namespace linkahead::transaction
#endif
