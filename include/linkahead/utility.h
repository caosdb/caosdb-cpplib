/*
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021-2024 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef LINKAHEAD_UTILS_H
#define LINKAHEAD_UTILS_H
#include "linkahead/data_type.h" // for AtomicDataType
#include "linkahead/entity.h"    // for Importance, Role
#include <cstdlib>               // for getenv
#include <filesystem>            // for path
#include <fstream>               // for basic_istream<>::__ist...
#include <memory>                // for shared_ptr
#include <string>                // for string, operator+, cha...

namespace linkahead::utility {
using std::ifstream;
using std::filesystem::exists;
using std::filesystem::path;

/**
 * @brief Get the name of the enum value.  May be useful for higher-order LinkAhead clients.
 */
template <typename Enum> auto getEnumNameFromValue(Enum v) -> std::string;

// Forward declaration of specializations
template <>
auto getEnumNameFromValue<linkahead::entity::AtomicDataType>(linkahead::entity::AtomicDataType v)
  -> std::string;
template <>
auto getEnumNameFromValue<linkahead::entity::Importance>(linkahead::entity::Importance v)
  -> std::string;
template <>
auto getEnumNameFromValue<linkahead::entity::Role>(linkahead::entity::Role v) -> std::string;

/**
 * @brief Get the enum value from a string.
 *
 * @detail May be useful for higher-order LinkAhead clients and only makes sense if specialized.
 */
template <typename Enum> auto getEnumValueFromName(const std::string &name) -> Enum;

// Forward declaration of specializations
template <>
auto getEnumValueFromName<linkahead::entity::AtomicDataType>(const std::string &name)
  -> linkahead::entity::AtomicDataType;
template <>
auto getEnumValueFromName<linkahead::entity::Importance>(const std::string &name)
  -> linkahead::entity::Importance;
template <>
auto getEnumValueFromName<linkahead::entity::Role>(const std::string &name)
  -> linkahead::entity::Role;

/**
 * @brief Read a text file into a string and return the file's content.
 */
auto load_string_file(const path &file_path) -> std::string;

/**
 * @brief Return the environment variable KEY, or FALLBACK if it does not exist.
 */
inline auto get_env_fallback(const char *key, const char *fallback) -> const char * {
  const char *val = getenv(key);
  if (val == nullptr) {
    return fallback;
  } else {
    return val;
  }
}

/**
 * @brief Return the value of an environment variable or - if undefined - the
 * fallback value.
 */
inline auto get_env_fallback(const std::string &key, const std::string &fallback)
  -> const std::string {
  const char *val = get_env_fallback(key.c_str(), fallback.c_str());

  auto const result = std::string(val);
  return result;
}

/**
 * @brief JsonValue is a thin wrapper around a implementation specific
 * third-party json object (e.g. boost).
 */
class JsonValue {
public:
  /**
   * Default Constructor.
   *
   * Creates an empty wrapper where `wrapped` is nullptr.
   */
  JsonValue() : JsonValue(nullptr) {}
  /**
   * Constructor.
   *
   * By calling this constructor the ownership of the `wrapped` parameter is
   * transferred to this object.
   */
  JsonValue(void *wrapped);
  /**
   * Destructor.
   *
   * Also deletes the `wrapped` object.
   */
  ~JsonValue();

  /**
   * Copy Constructor.
   *
   * Also copies the `wrapped` object.
   */
  JsonValue(const JsonValue &other);

  /**
   * Copy Assigment.
   *
   * Also copies the `wrapped` object.
   */
  auto operator=(const JsonValue &other) -> JsonValue &;

  /**
   * Move Constructor.
   *
   * Also moves the `wrapped` object.
   */
  JsonValue(JsonValue &&other) noexcept;

  /**
   * Move Assigment.
   *
   * Also moves the `wrapped` object.
   */
  auto operator=(JsonValue &&other) noexcept -> JsonValue &;

  /**
   * Reset this object.
   *
   * Also deletes `wrapped` sets it to the nullptr.
   */
  auto Reset() -> void;

  /**
   * An object which represents a JSON value. The object's class is an
   * implementation detail.
   */
  std::shared_ptr<void> wrapped;
};

/**
 * @brief Load json object from a json file and return it.
 */
auto load_json_file(const path &json_file) -> JsonValue;

/**
 * @brief Encode string as base64
 */
auto base64_encode(const std::string &plain) -> std::string;

inline auto get_home_directory() -> const path {
#if defined(_WIN32)
  const auto *const home = getenv("USERPROFILE");
#else
  const auto *const home = getenv("HOME");
#endif

  return home;
}

} // namespace linkahead::utility
#endif
