/*
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021-2024 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef LINKAHEAD_STATUS_CODE_H
#define LINKAHEAD_STATUS_CODE_H

#include <grpcpp/impl/codegen/status_code_enum.h> // for StatusCode
#include <string>                                 // for string

namespace linkahead {

/**
 * StatusCodes represent the status of this client, it's connections,
 * configuration and so on.
 *
 * In contrast to MessageCodes, these status codes do not represent the status
 * of the entities of a transaction or of the server (or only inasmuch the
 * GENERIC_TRANSACTION_ERROR indicates that *there are* errors in a
 * transaction).
 */
enum StatusCode {
  READY = -4,
  GO_ON = -3,
  INITIAL = -2,
  EXECUTING = -1,
  SUCCESS = grpc::StatusCode::OK,                              // = 0
  CANCELLED = grpc::StatusCode::CANCELLED,                     // = 1
  UNKNOWN = grpc::StatusCode::UNKNOWN,                         // = 2
  INVALID_ARGUMENT = grpc::StatusCode::INVALID_ARGUMENT,       // = 3
  DEADLINE_EXCEEDED = grpc::StatusCode::DEADLINE_EXCEEDED,     // = 4
  NOT_FOUND = grpc::StatusCode::NOT_FOUND,                     // = 5
  ALREADY_EXISTS = grpc::StatusCode::ALREADY_EXISTS,           // = 6
  PERMISSION_DENIED = grpc::StatusCode::PERMISSION_DENIED,     // = 7
  RESOURCE_EXHAUSTED = grpc::StatusCode::RESOURCE_EXHAUSTED,   // = 8
  FAILED_PRECONDITION = grpc::StatusCode::FAILED_PRECONDITION, // = 9
  ABORTED = grpc::StatusCode::ABORTED,                         // = 10
  OUT_OF_RANGE = grpc::StatusCode::OUT_OF_RANGE,               // = 11
  UNIMPLEMENTED = grpc::StatusCode::UNIMPLEMENTED,             // = 12
  INTERNAL = grpc::StatusCode::INTERNAL,                       // = 13
  CONNECTION_ERROR = grpc::StatusCode::UNAVAILABLE,            // = 14
  DATA_LOSS = grpc::StatusCode::DATA_LOSS,                     // = 15
  AUTHENTICATION_ERROR = grpc::StatusCode::UNAUTHENTICATED,    // = 16
  GENERIC_RPC_ERROR = 20,
  GENERIC_ERROR = 21,
  GENERIC_TRANSACTION_ERROR = 22,
  CONFIGURATION_ERROR = 23,
  CONNECTION_CONFIGURATION_ERROR = 24,
  TRANSACTION_STATUS_ERROR = 25,
  TRANSACTION_TYPE_ERROR = 26,
  UNSUPPORTED_FEATURE = 27,
  ORIGINAL_ENTITY_MISSING_ID = 28,
  // EXTERN_C_ASSIGNMENT_ERROR = 29,
  ENTITY_CANNOT_HAVE_A_DATA_TYPE = 30,
  ENTITY_CANNOT_HAVE_A_VALUE = 31,
  NOT_A_FILE_ENTITY = 32,
  PATH_IS_A_DIRECTORY = 33,
  FILE_DOES_NOT_EXIST_LOCALLY = 34,
  FILE_UPLOAD_ERROR = 35,
  FILE_DOWNLOAD_ERROR = 36,
  ENUM_MAPPING_ERROR = 37,
  SPOILED = 38,
  OTHER_CLIENT_ERROR = 9999,
};

auto get_status_description(int code) -> const std::string &;

} // namespace linkahead
#endif
