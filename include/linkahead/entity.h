/*
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021 Florian Spreckelsen <f.spreckelsen@indiscale.com>
 * Copyright (C) 2021-2024 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

/**
 * @brief Anything entity-related.
 * @file linkahead/entity.h
 * @author Timm Fitchen
 * @date 2021-07-07
 */
#ifndef LINKAHEAD_ENTITY_H
#define LINKAHEAD_ENTITY_H

#include <google/protobuf/message.h>   // for RepeatedPtrField
#include <cstdint>                     // for int64_t
#include <filesystem>                  // for path, exists, is_directory
#include <iosfwd>                      // for ptrdiff_t
#include <iterator>                    // for output_iterator_tag
#include <map>                         // for map
#include <stdexcept>                   // for out_of_range
#include <string>                      // for string, basic_string, operator==
#include <utility>                     // for move, pair
#include <vector>                      // for vector
#include "caosdb/entity/v1/main.pb.h"  // for Entity, EntityRole, Property
#include "linkahead/data_type.h"       // for DataType, AtomicDataType
#include "linkahead/file_descriptor.h" // for FileDescriptor
#include "linkahead/logging.h"         // for LoggerOutputStream, LINKAHEAD...
#include "linkahead/message_code.h"    // for get_message_code, MessageCode
#include "linkahead/protobuf_helper.h" // for ScalarProtoMessageWrapper
#include "linkahead/status_code.h"     // for StatusCode
#include "linkahead/value.h"           // for Value, AbstractValue, Arena
// IWYU pragma: no_include "net/proto2/public/repeated_field.h"

namespace linkahead::entity {
using caosdb::entity::v1::IdResponse;
using std::filesystem::exists;
using std::filesystem::is_directory;
using ProtoParent = caosdb::entity::v1::Parent;
using ProtoProperty = caosdb::entity::v1::Property;
using ProtoEntity = caosdb::entity::v1::Entity;
using ProtoMessage = caosdb::entity::v1::Message;
using ProtoValue = caosdb::entity::v1::Value;
using ProtoDataType = caosdb::entity::v1::DataType;
using caosdb::entity::v1::EntityRole;
using ProtoImportance = caosdb::entity::v1::Importance;
using caosdb::entity::v1::EntityResponse;
using caosdb::entity::v1::FileTransmissionId;
using ::google::protobuf::RepeatedPtrField;
using google::protobuf::RepeatedPtrField;
using linkahead::StatusCode;
using linkahead::utility::get_arena;
using linkahead::utility::ProtoMessageWrapper;

static const std::string logger_name = "linkahead::entity";

/**
 * The property importance.
 */
enum class Importance {
  UNSPECIFIED = ProtoImportance::IMPORTANCE_UNSPECIFIED, ///< Unset/None
  OBLIGATORY = ProtoImportance::IMPORTANCE_OBLIGATORY,   ///< Obligatory importance.
  RECOMMENDED = ProtoImportance::IMPORTANCE_RECOMMENDED, ///< Recommended importance.
  SUGGESTED = ProtoImportance::IMPORTANCE_SUGGESTED,     ///< Suggested importance.
  FIX = ProtoImportance::IMPORTANCE_FIX,                 ///< Fix importance.
};
const std::map<Importance, std::string> importance_names = {
  {Importance::UNSPECIFIED, "UNSPECIFIED"},
  {Importance::OBLIGATORY, "OBLIGATORY"},
  {Importance::RECOMMENDED, "RECOMMENDED"},
  {Importance::SUGGESTED, "SUGGESTED"},
  {Importance::FIX, "FIX"}};

/**
 * The entity role.
 */
enum class Role {
  UNSPECIFIED = EntityRole::ENTITY_ROLE_UNSPECIFIED, ///< Unset/None
  RECORD_TYPE = EntityRole::ENTITY_ROLE_RECORD_TYPE, ///< RecordType
  RECORD = EntityRole::ENTITY_ROLE_RECORD,           ///< Record
  PROPERTY = EntityRole::ENTITY_ROLE_PROPERTY,       ///< Property
  FILE = EntityRole::ENTITY_ROLE_FILE,               ///< File
};
const std::map<Role, std::string> role_names = {{Role::UNSPECIFIED, "UNSPECIFIED"},
                                                {Role::RECORD_TYPE, "RECORD_TYPE"},
                                                {Role::RECORD, "RECORD"},
                                                {Role::PROPERTY, "PROPERTY"},
                                                {Role::FILE, "FILE"}};

/**
 * Abstract base class for Messages, Properties and Parents container classes.
 *
 * This is a list-like class.
 *
 * This class wraps a RepeatedPtrField.
 */
template <typename T, typename P>
class RepeatedPtrFieldWrapper : public ProtoMessageWrapper<RepeatedPtrField<P>> {
  class iterator;

public:
  /**
   * Return the current size of the container.
   */
  [[nodiscard]] inline auto size() const -> int { return this->wrapped->size(); }
  /**
   * Return a const reference to the element at the given index.
   */
  [[nodiscard]] inline auto at(int index) const -> const T & { return *mutable_at(index); }

  /**
   * Return a mutable pointer to the element at the given index.
   */
  [[nodiscard]] inline auto mutable_at(int index) const -> T * {
    if (index >= size() || index < 0) {
      throw std::out_of_range("Container has size " + std::to_string(size()));
    }
    if (cache.count(index) == 0) {
      cache.emplace(index, T(this->wrapped->Mutable(index)));
    }
    return &(cache.at(index));
  }

  /**
   * Return iterator positioned at the beginning of the list.
   */
  auto begin() -> iterator;
  /**
   * Return iterator positioned at the end of the list.
   */
  auto end() -> iterator;
  /**
   * Return constant iterator positioned at the beginning of the list.
   */
  auto begin() const -> const iterator;
  /**
   * Return constant iterator positioned at the end of the list.
   */
  auto end() const -> const iterator;

  friend class Entity;

  virtual ~RepeatedPtrFieldWrapper(){};

  inline auto ToString() const noexcept -> const std::string override {
    if (this->size() == 0) {
      return "[]\n";
    }
    std::string result("[\n");
    for (int i = 0; i < this->size();) {
      LINKAHEAD_DEBUG_MESSAGE_STRING(this->wrapped->at(i), next);
      result += next;
      if (++i < this->size()) {
        result.replace(result.size() - 1, 1, ",\n");
      }
    }

    return result.append(std::string("]\n"));
  }

  /**
   * Return true if the underlying Protobuf messages have the same
   * serialization.
   */
  inline auto operator==(const RepeatedPtrFieldWrapper<T, P> &other) const noexcept -> bool {
    if (this->wrapped != nullptr && other.wrapped != nullptr && this->size() == other.size()) {
      for (int i = 0; i < this->size(); i++) {
        if (this->wrapped->Get(i).SerializeAsString() !=
            other.wrapped->Get(i).SerializeAsString()) {
          return false;
        }
      }
      return true;
    }
    // last chance for "true": both nullptr?
    return this->wrapped == other.wrapped;
  }

  /**
   * Return true if the underlying Protobuf messages have a different
   * serialization.
   */
  inline auto operator!=(const RepeatedPtrFieldWrapper<T, P> &other) const noexcept -> bool {
    return !(*this == other);
  }

protected:
  RepeatedPtrFieldWrapper() : ProtoMessageWrapper<RepeatedPtrField<P>>(){};
  explicit inline RepeatedPtrFieldWrapper(RepeatedPtrField<P> *wrapped)
    : ProtoMessageWrapper<RepeatedPtrField<P>>(wrapped) {}

  /**
   * Append an element. This adds the element to the end of the wrapped list
   * and increases the size by one.
   */
  inline auto Append(const T &element) -> void {
    auto *destination = this->wrapped->Add();
    destination->CopyFrom(*element.wrapped);
  }

  /**
   * Remove the element at the given index.
   */
  inline auto Remove(int index) -> void {
    this->wrapped->DeleteSubrange(index, 1);
    if (cache.count(index) > 0) {
      cache.erase(index);
    }

    // shift all indices in the cache above index (such that the values do not
    // get deleted/copied because this could destroy pointers (c-interface).
    for (int i = index + 1; i < size(); i++) {
      if (cache.count(i) > 0) {
        auto handle = cache.extract(i);
        handle.key()--;
        cache.insert(std::move(handle));
      }
    }
    // this is possible because cache is an ordered map
    cache.erase(cache.find(size()), cache.end());
  }

  inline auto Clear() noexcept -> void { this->wrapped->Clear(); }

  mutable std::map<int, T> cache;

private:
  class iterator {
  public:
    using iterator_category = std::output_iterator_tag;
    using value_type = T;
    using difference_type = std::ptrdiff_t;
    using pointer = T *;
    using reference = T &;

    explicit iterator(const RepeatedPtrFieldWrapper<T, P> *instance, int index = 0);
    // TODO(henrik) add unit tests
    auto operator*() const -> T &;
    auto operator++() -> iterator &;
    auto operator++(int) -> iterator;
    auto operator!=(const iterator &rhs) const -> bool;

  private:
    int current_index = 0;
    const RepeatedPtrFieldWrapper<T, P> *instance;
  };
};

template <class T, class P>
RepeatedPtrFieldWrapper<T, P>::iterator::iterator(const RepeatedPtrFieldWrapper<T, P> *instance,
                                                  int index)
  : current_index(index), instance(instance) {}

template <typename T, typename P>
auto RepeatedPtrFieldWrapper<T, P>::iterator::operator*() const -> T & {
  return *(this->instance->mutable_at(current_index));
}

template <typename T, typename P>
auto RepeatedPtrFieldWrapper<T, P>::iterator::operator++()
  -> RepeatedPtrFieldWrapper<T, P>::iterator & {
  current_index++;
  return *this;
}

template <typename T, typename P>
auto RepeatedPtrFieldWrapper<T, P>::iterator::operator++(int)
  -> RepeatedPtrFieldWrapper<T, P>::iterator {
  iterator tmp(*this);
  operator++();
  return tmp;
}

template <typename T, typename P>
auto RepeatedPtrFieldWrapper<T, P>::iterator::operator!=(const iterator &rhs) const -> bool {
  return this->current_index != rhs.current_index;
}

template <typename T, typename P>
auto RepeatedPtrFieldWrapper<T, P>::begin() -> RepeatedPtrFieldWrapper<T, P>::iterator {
  return RepeatedPtrFieldWrapper<T, P>::iterator(this, 0);
}

template <typename T, typename P>
auto RepeatedPtrFieldWrapper<T, P>::end() -> RepeatedPtrFieldWrapper<T, P>::iterator {
  return RepeatedPtrFieldWrapper<T, P>::iterator(this, size());
}

template <typename T, typename P>
auto RepeatedPtrFieldWrapper<T, P>::begin() const -> const RepeatedPtrFieldWrapper<T, P>::iterator {
  return RepeatedPtrFieldWrapper<T, P>::iterator(this, 0);
}

template <typename T, typename P>
auto RepeatedPtrFieldWrapper<T, P>::end() const -> const RepeatedPtrFieldWrapper<T, P>::iterator {
  return RepeatedPtrFieldWrapper<T, P>::iterator(this, size());
}

/**
 * Messages convey information about the state of entities and result of
 * transactions.
 *
 * A Message object can be thought of as kinf of a generalized error object in
 * other frameworks. Please have a look at MessageCodes for more details.
 */
class Message : public ScalarProtoMessageWrapper<ProtoMessage> {
public:
  /**
   * Get the code of this message.
   *
   * The message code is a unique identifier of the type of message and is
   * intended to make it easiert to identify messages in a machine-readable
   * way.
   */
  [[nodiscard]] inline auto GetCode() const -> MessageCode {
    return get_message_code(wrapped->code());
  }
  /**
   * Get the description of this message.
   *
   * The description is intended for a human reader.
   */
  [[nodiscard]] inline auto GetDescription() const -> std::string { return wrapped->description(); }

  friend class Entity;
  // TODO(fspreck) Re-enable once we have decided how messages are
  // appended to parents and properties
  // friend class Parent;
  // friend class Property;
  friend class Messages;
  friend class RepeatedPtrFieldWrapper<Message, ProtoMessage>;

private:
  explicit inline Message(ProtoMessage *wrapped)
    : ScalarProtoMessageWrapper<ProtoMessage>(wrapped){};
};

/**
 * Container for Messages.
 */
class Messages : public RepeatedPtrFieldWrapper<Message, ProtoMessage> {
public:
  friend class Entity;
  // TODO(fspreck) Same here.
  // friend class Parent;
  // friend class Property;

private:
  inline Messages() : RepeatedPtrFieldWrapper<Message, ProtoMessage>(){};
};

///////////////////////////////////////////////////////////////////////////////
// class Parent ///////////////////////////////////////////////////////////////

/**
 * Parent of an Entity.
 *
 * This implementation uses protobuf messages as storage backends. In other
 * words, this class wraps a protobuf message and provides getter and setter
 * methods.
 */
class Parent : public ScalarProtoMessageWrapper<ProtoParent> {
public:
  explicit inline Parent(ProtoParent *wrapped) : ScalarProtoMessageWrapper<ProtoParent>(wrapped){};
  Parent() : ScalarProtoMessageWrapper<ProtoParent>(){};
  ~Parent() = default;

  /**
   * Copy constructor.
   */
  inline Parent(const Parent &other)
    : Parent(ProtoMessageWrapper<ProtoParent>::CopyProtoMessage(other.wrapped)) {}

  /**
   * Move constructor.
   */
  inline Parent(Parent &&other) : Parent(other.wrapped) { other.wrapped = nullptr; }

  /**
   * Copy assignment operator.
   */
  inline auto operator=(const Parent &other) -> Parent & {
    if (this != &other) {
      this->wrapped->CopyFrom(*other.wrapped);
    }
    return *this;
  }

  /**
   * Move assignment operator.
   */
  inline auto operator=(Parent &&other) -> Parent & {
    this->wrapped = other.wrapped;
    other.wrapped = nullptr;
    return *this;
  }

  /**
   * Return the id of the parent entity.
   */
  [[nodiscard]] auto GetId() const -> const std::string &;
  /**
   * Return the name of the parent entity.
   */
  [[nodiscard]] auto GetName() const -> const std::string &;
  /**
   * Return the description of the parent entity.
   */
  [[nodiscard]] auto GetDescription() const -> const std::string &;

  /**
   * Set the id of the parent.
   */
  auto SetId(const std::string &id) -> void;
  /**
   * Set the name of the parent.
   */
  auto SetName(const std::string &name) -> void;

  // TODO(fspreck) Finish the following implementations once we have
  // decided how to attach messages to parents.
  // /**
  //  * Return the error messages of this parent.
  //  */
  // [[nodiscard]] inline auto GetErrors() const -> const Messages & {
  //   return errors;
  // }
  // [[nodiscard]] inline auto HasErrors() const -> bool {
  //   return this->errors.wrapped->size() > 0;
  // }
  // /**
  //  * Return the warning messages of this parent.
  //  */
  // [[nodiscard]] inline auto GetWarnings() const -> const Messages & {
  //   return warnings;
  // }
  // [[nodiscard]] inline auto HasWarnings() const -> bool {
  //   return this->warnings.wrapped->size() > 0;
  // }
  // /**
  //  * Return the info messages of this parent.
  //  */
  // [[nodiscard]] inline auto GetInfos() const -> const Messages & {
  //   return infos;
  // }

  friend class Entity;
  friend class Parents;
  friend class RepeatedPtrFieldWrapper<Parent, ProtoParent>;

private:
  // Messages errors;
  // Messages warnings;
  // Messages infos;
};

/**
 * Container for parents of entities.
 *
 * Should only be instantiated and write-accessed by the owning entity.
 */
class Parents : public RepeatedPtrFieldWrapper<Parent, ProtoParent> {
public:
  friend class Entity;

private:
  inline Parents() : RepeatedPtrFieldWrapper<Parent, ProtoParent>(){};
  explicit inline Parents(::google::protobuf::RepeatedPtrField<caosdb::entity::v1::Parent> *wrapped)
    : RepeatedPtrFieldWrapper<Parent, ProtoParent>(wrapped){};
};

///////////////////////////////////////////////////////////////////////////////
// class Property /////////////////////////////////////////////////////////////

/**
 * Property of an Entity.
 *
 * This is a property which belongs to another entity. Don't confuse it with
 * an Entity with the "Property" role.
 */
class Property : public ScalarProtoMessageWrapper<ProtoProperty> {
public:
  /**
   * Copy constructor.
   */
  inline Property(const Property &other)
    : Property(ProtoMessageWrapper<ProtoProperty>::CopyProtoMessage(other.wrapped)) {
    LINKAHEAD_LOG_TRACE(logger_name) << "Property::Property(const Property &) "
                                     << "- Copy constructor";
  };

  /**
   * Move constructor.
   */
  inline Property(Property &&other) : Property(other.wrapped) {
    LINKAHEAD_LOG_TRACE(logger_name) << "Property::Property(Property  &&) "
                                     << "- Move constructor";
    other.wrapped = nullptr;
    other.data_type.wrapped = nullptr;
    other.value.wrapped = nullptr;
  }

  explicit inline Property(ProtoProperty *other)
    : ScalarProtoMessageWrapper<ProtoProperty>(other),
      value(this->wrapped->has_value() ? this->wrapped->mutable_value()
                                       : static_cast<ProtoValue *>(nullptr)),
      data_type(this->wrapped->has_data_type() ? this->wrapped->mutable_data_type()
                                               : static_cast<ProtoDataType *>(nullptr)){};
  inline Property()
    : ScalarProtoMessageWrapper<ProtoProperty>(), value(static_cast<ProtoValue *>(nullptr)),
      data_type(static_cast<ProtoDataType *>(nullptr)){};

  ~Property() = default;

  /**
   * Return the id of this  property
   */
  [[nodiscard]] auto GetId() const -> const std::string &;
  /**
   * Return the name of this  property
   */
  [[nodiscard]] auto GetName() const -> const std::string &;
  /**
   * Return the description of this  property
   */
  [[nodiscard]] auto GetDescription() const -> const std::string &;
  /**
   * Return the importance of this  property
   */
  [[nodiscard]] auto GetImportance() const -> Importance;
  /**
   * Return the value of this  property
   */
  [[nodiscard]] auto GetValue() const -> const Value &;
  /**
   * Return the unit of this  property
   */
  [[nodiscard]] auto GetUnit() const -> const std::string &;
  /**
   * Return the datatype of this  property
   */
  [[nodiscard]] auto GetDataType() const -> const DataType &;
  // TODO(fspreck) Implement these when we have decided how to attach
  // messages to properties.
  // [[nodiscard]] auto GetErrors() const -> const Messages &;
  // [[nodiscard]] auto GetWarnings() const -> const Messages &;
  // [[nodiscard]] auto GetInfos() const -> const Messages &;

  /**
   * Set the id of this property.
   */
  auto SetId(const std::string &id) -> void;
  /**
   * Set the name of this property.
   */
  auto SetName(const std::string &name) -> void;
  /**
   * Set the description of this property.
   */
  auto SetDescription(const std::string &description) -> void;
  /**
   * Set the importance of this property.
   */
  auto SetImportance(Importance importance) -> void;
  /**
   * Set the value of this property.
   */
  auto SetValue(const Value &value) -> StatusCode;
  auto SetValue(const AbstractValue &value) -> StatusCode;
  auto SetValue(const std::string &value) -> StatusCode;
  auto SetValue(const char *value) -> StatusCode;
  auto SetValue(const double value) -> StatusCode;
  auto SetValue(const std::vector<std::string> &values) -> StatusCode;
  auto SetValue(const std::vector<char *> &values) -> StatusCode;
  auto SetValue(const std::vector<int64_t> &values) -> StatusCode;
  auto SetValue(const std::vector<int> &values) -> StatusCode;
  auto SetValue(const std::vector<double> &values) -> StatusCode;
  auto SetValue(const std::vector<bool> &values) -> StatusCode;
  auto SetValue(const int64_t value) -> StatusCode;
  auto SetValue(const int value) -> StatusCode;
  auto SetValue(const bool value) -> StatusCode;

  /**
   * Set the unit of this property.
   */
  auto SetUnit(const std::string &unit) -> void;
  /**
   * Set the datatype of this property.
   */
  auto SetDataType(const DataType &new_data_type) -> StatusCode;
  auto SetDataType(const AtomicDataType new_data_type, bool list_type = false) -> StatusCode;
  auto SetDataType(const std::string &new_data_type, bool list_type = false) -> StatusCode;

  /**
   * Copy assignment operator.
   */
  auto operator=(const Property &other) -> Property & {
    LINKAHEAD_LOG_TRACE(logger_name) << "Property::operator=(const Property &) "
                                     << "- Copy assignment operator";
    this->wrapped->CopyFrom(*other.wrapped);

    this->value.wrapped = (this->wrapped->has_value() ? this->wrapped->mutable_value()
                                                      : static_cast<ProtoValue *>(nullptr));
    this->data_type.wrapped =
      (this->wrapped->has_data_type() ? this->wrapped->mutable_data_type()
                                      : static_cast<ProtoDataType *>(nullptr));
    return *this;
  }

  /**
   * Move assignment operator.
   */
  auto operator=(Property &&other) -> Property & {
    LINKAHEAD_LOG_TRACE(logger_name) << "Property::operator=(Property &&) "
                                     << "- Move assignment operator";
    this->wrapped = other.wrapped;
    other.wrapped = nullptr;
    this->value = std::move(other.value);
    this->data_type = std::move(other.data_type);
    return *this;
  }

  friend class Entity;
  friend class Properties;
  friend class RepeatedPtrFieldWrapper<Property, ProtoProperty>;

private:
  Value value;
  DataType data_type;
};

/**
 * Container for Properties of Entities.
 *
 * Should only be instantiated and write-accessed by the owning entity.
 *
 * Note that iterating over the Property contents only works via references,
 * since the Property copy constructor is deliberately disabled:
 *
 * \code
 * // Accessing single properties as reference
 * auto &property = my_properties.at(0);
 * // Iterating via reference
 * for (auto &property : my_properties) {...}
 * \endcode
 */
class Properties : public RepeatedPtrFieldWrapper<Property, ProtoProperty> {
public:
  friend class Entity;

private:
  inline Properties(){};
  explicit inline Properties(RepeatedPtrField<ProtoProperty> *wrapped)
    : RepeatedPtrFieldWrapper<Property, ProtoProperty>(wrapped){};
};

///////////////////////////////////////////////////////////////////////////////
// class Entity ///////////////////////////////////////////////////////////////

/**
 * Entity is the central and basic data object of LinkAhead.
 *
 * This class is a wrapper of the Entity class auto-generated by protobuf
 * (henceforth "ProtoEntity").
 *
 * Overview of the Constructors:
 *
 * <li> Entity() - Calls Entity(ProtoEntity *) with a fresh ProtoEntity
 * <li> Entity(const Entity&) - Copy constructor, calls Entity(ProtoEntity *) after
 *   copying wrapped ProtoEntity of the original, then also copies all Messages.
 * <li> Entity(ProtoEntity *) - The workhorse of the constructors. Initializes
 *   everything and does not call other Entity constructors.
 * <li> Entity(EntityResponse *) - Constructor which is used by the Transaction class
 *   to create an Entity from the server's response, calls Entity(ProtoEntity).
 * <li> Entity(IdResponse *) - Constructor which is used by the Transaction
 *   class to create an Entity from the servers's response. calls Entity(),
 *   then moves the data to the wrapped ProtoEntity.
 * <li> Entity(Entity&&) - Move constructor, calls Entity(ProtoEntity *), then
 *   moves the messages and resets the original,
 */
class Entity : public ScalarProtoMessageWrapper<ProtoEntity> {
public:
  /**
   * Copy constructor.
   */
  inline Entity(const Entity &original)
    : Entity(ProtoMessageWrapper::CopyProtoMessage(original.wrapped)) {
    this->errors.wrapped->CopyFrom(*original.errors.wrapped);
    this->warnings.wrapped->CopyFrom(*original.warnings.wrapped);
    this->infos.wrapped->CopyFrom(*original.infos.wrapped);
  };
  explicit Entity(ProtoEntity *other)
    : ScalarProtoMessageWrapper<ProtoEntity>(other),
      value(this->wrapped->has_value() ? this->wrapped->mutable_value()
                                       : static_cast<ProtoValue *>(nullptr)),
      data_type(this->wrapped->has_data_type() ? this->wrapped->mutable_data_type()
                                               : static_cast<ProtoDataType *>(nullptr)) {
    properties.wrapped = this->wrapped->mutable_properties();
    parents.wrapped = this->wrapped->mutable_parents();
    if (this->wrapped->has_file_descriptor()) {
      file_descriptor.wrapped = this->wrapped->mutable_file_descriptor();
    }
  };
  explicit inline Entity(EntityResponse *response) : Entity(response->mutable_entity()) {
    this->errors.wrapped->Swap(response->mutable_errors());
    this->warnings.wrapped->Swap(response->mutable_warnings());
    this->infos.wrapped->Swap(response->mutable_infos());
  };

  explicit inline Entity(IdResponse *id_response) : Entity() {
    this->wrapped->set_id(id_response->id());
    this->wrapped->mutable_version()->Swap(id_response->mutable_version());
    this->errors.wrapped->Swap(id_response->mutable_errors());
    this->warnings.wrapped->Swap(id_response->mutable_warnings());
    this->infos.wrapped->Swap(id_response->mutable_infos());
  };

  explicit inline Entity()
    : ScalarProtoMessageWrapper<ProtoEntity>(), value(static_cast<ProtoValue *>(nullptr)),
      data_type(static_cast<ProtoDataType *>(nullptr)) {
    properties.wrapped = this->wrapped->mutable_properties();
    parents.wrapped = this->wrapped->mutable_parents();
  };

  ~Entity() = default;

  /**
   * Move constructor.
   */
  explicit inline Entity(Entity &&original) : Entity(original.wrapped) {
    original.wrapped = nullptr;
    original.value.wrapped = nullptr;
    original.data_type.wrapped = nullptr;
    original.properties.wrapped = nullptr;
    original.parents.wrapped = nullptr;
    this->errors = std::move(original.errors);
    this->warnings = std::move(original.warnings);
    this->infos = std::move(original.infos);
  };

  /**
   * Move assignment operator.
   */
  auto operator=(Entity &&other) -> Entity & {
    this->wrapped = other.wrapped;
    other.wrapped = nullptr;
    this->data_type = std::move(other.data_type);
    this->value = std::move(other.value);
    this->properties = std::move(other.properties);
    this->parents = std::move(other.parents);
    this->file_descriptor = std::move(other.file_descriptor);
    this->errors = std::move(other.errors);
    this->warnings = std::move(other.warnings);
    this->infos = std::move(other.infos);
    return *this;
  }

  /**
   * Copy assignment operator.
   */
  auto operator=(const Entity &other) -> Entity & {
    this->wrapped->CopyFrom(*other.wrapped);
    this->data_type = other.data_type;
    this->value = other.value;
    this->properties = other.properties;
    this->parents = other.parents;
    this->file_descriptor.local_path = std::filesystem::path(other.file_descriptor.local_path);
    this->file_descriptor.file_transmission_id->CopyFrom(
      *other.file_descriptor.file_transmission_id);
    this->file_descriptor.wrapped->CopyFrom(*other.file_descriptor.wrapped);
    this->errors = other.errors;
    this->warnings = other.warnings;
    this->infos = other.infos;
    return *this;
  }

  [[nodiscard]] inline auto GetId() const noexcept -> const std::string & {
    return this->wrapped->id();
  };
  [[nodiscard]] inline auto HasId() const noexcept -> bool { return !this->wrapped->id().empty(); }
  [[nodiscard]] inline auto GetVersionId() const -> const std::string & {
    return this->wrapped->version().id();
  };

  [[nodiscard]] inline auto GetRole() const -> Role {
    return static_cast<Role>(this->wrapped->role());
  };
  [[nodiscard]] inline auto GetName() const -> const std::string & {
    return this->wrapped->name();
  };
  [[nodiscard]] inline auto GetDescription() const -> const std::string & {
    return this->wrapped->description();
  };

  [[nodiscard]] inline auto GetDataType() const -> const DataType & { return this->data_type; };
  [[nodiscard]] inline auto GetUnit() const -> const std::string & {
    return this->wrapped->unit();
  };
  [[nodiscard]] inline auto GetValue() const -> const Value & { return this->value; };

  [[nodiscard]] auto GetParents() const -> const Parents &;
  // TODO(henrik) const prevents properties from being changed
  // what about an interface that operates on the list directly?
  [[nodiscard]] auto GetProperties() const -> const Properties &;
  [[nodiscard]] inline auto GetErrors() const -> const Messages & { return errors; }
  [[nodiscard]] inline auto HasErrors() const -> bool { return this->errors.wrapped->size() > 0; }
  [[nodiscard]] auto GetWarnings() const -> const Messages & { return warnings; }
  [[nodiscard]] inline auto HasWarnings() const -> bool {
    return this->warnings.wrapped->size() > 0;
  }
  [[nodiscard]] auto GetInfos() const -> const Messages & { return infos; }
  [[nodiscard]] inline auto HasInfos() const -> bool { return this->infos.wrapped->size() > 0; }

  auto SetRole(Role role) -> void;
  auto SetName(const std::string &name) -> void;
  /**
   * Set the description of this entity.
   */
  auto SetDescription(const std::string &description) -> void;

  auto SetValue(const AbstractValue &value) -> StatusCode;
  auto SetValue(const Value &value) -> StatusCode;
  auto SetValue(const std::string &value) -> StatusCode;
  auto SetValue(const char *value) -> StatusCode;
  auto SetValue(const double value) -> StatusCode;
  auto SetValue(const std::vector<std::string> &values) -> StatusCode;
  auto SetValue(const std::vector<char *> &values) -> StatusCode;
  auto SetValue(const std::vector<int64_t> &values) -> StatusCode;
  auto SetValue(const std::vector<int> &values) -> StatusCode;
  auto SetValue(const std::vector<double> &values) -> StatusCode;
  auto SetValue(const std::vector<bool> &values) -> StatusCode;
  auto SetValue(const int64_t value) -> StatusCode;
  auto SetValue(const int value) -> StatusCode;
  auto SetValue(const bool value) -> StatusCode;

  auto SetUnit(const std::string &unit) -> void;

  auto SetDataType(const DataType &new_data_type) -> StatusCode;
  auto SetDataType(const AtomicDataType new_data_type, bool list_type = false) -> StatusCode;
  auto SetDataType(const std::string &new_data_type, bool list_type = false) -> StatusCode;

  auto AppendProperty(const Property &property) -> void;
  auto RemoveProperty(int index) -> void;

  auto AppendParent(const Parent &parent) -> void;
  auto RemoveParent(int index) -> void;
  /**
   * Copy all of this entity's features to the target ProtoEntity.
   */
  auto CopyTo(ProtoEntity *target) -> void;

  auto SetFilePath(const std::string &path) -> void;
  inline auto HasFile() const -> bool { return !this->file_descriptor.local_path.empty(); }
  inline auto SetFileTransmissionId(FileTransmissionId *file_transmission_id) -> void {
    file_descriptor.file_transmission_id = file_transmission_id;
  }

  inline auto GetFileDescriptor() -> FileDescriptor & { return this->file_descriptor; }

  inline auto GetLocalPath() const noexcept -> const std::filesystem::path & {
    return this->file_descriptor.local_path;
  }

  inline auto SetLocalPath(const std::filesystem::path &local_path) noexcept -> StatusCode {
    if (GetRole() != Role::FILE) {
      LINKAHEAD_LOG_WARN(logger_name) << "Entity::SetLocalPath failed. This is not a file entity.";
      return StatusCode::NOT_A_FILE_ENTITY;
    }
    if (!exists(local_path)) {
      LINKAHEAD_LOG_WARN(logger_name)
        << "Entity::SetLocalPath failed. This file does not exists: " << local_path.string();
      return StatusCode::FILE_DOES_NOT_EXIST_LOCALLY;
    }
    if (is_directory(local_path)) {
      LINKAHEAD_LOG_WARN(logger_name)
        << "Entity::SetLocalPath failed. This file is a directory: " << local_path.string();
      return StatusCode::PATH_IS_A_DIRECTORY;
    }

    LINKAHEAD_LOG_TRACE(logger_name) << "Entity::SetLocalPath(" << local_path.string() << ");";
    this->file_descriptor.local_path = local_path;
    return StatusCode::SUCCESS;
  }

  inline auto ClearMessages() noexcept -> void {
    errors.Clear();
    warnings.Clear();
    infos.Clear();
  }

  /**
   * Return true if the other entity is equal to to this entity.
   *
   * This method ignores the errors, warnings and info messages.
   */
  inline auto operator==(const Entity &other) const noexcept -> bool {
    return this->wrapped->SerializeAsString() == other.wrapped->SerializeAsString();
  }

private:
  static auto CreateMessagesField() -> RepeatedPtrField<ProtoMessage> *;
  auto SetId(const std::string &id) -> void;
  auto SetVersionId(const std::string &id) -> void;
  auto inline GetArena() const -> Arena * { return get_arena(); }

private:
  FileDescriptor file_descriptor;
  Properties properties;
  Parents parents;
  Messages errors;
  Messages warnings;
  Messages infos;
  Value value;
  DataType data_type;
};

} // namespace linkahead::entity
#endif
