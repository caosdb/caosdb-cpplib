#pragma once
#include "caosdb/entity/v1/main.pb.h"        // for FileTransmissionS...
#include "caosdb/entity/v1/main.grpc.pb.h"   // for FileDownloadResponse
#include "linkahead/handler_interface.h"     // for HandlerTag
#include "linkahead/unary_rpc_handler.h"     // for HandlerTag, Handl...
#include <grpcpp/completion_queue.h>         // for CompletionQueue
#include <grpcpp/support/async_unary_call.h> // for ClientAsyncResponseReader
#include <memory>                            // for unique_ptr

namespace linkahead::transaction {

using caosdb::entity::v1::EntityTransactionService;
using caosdb::entity::v1::MultiTransactionRequest;
using caosdb::entity::v1::MultiTransactionResponse;

class EntityTransactionHandler final : public UnaryRpcHandler {
public:
  EntityTransactionHandler(HandlerTag tag, EntityTransactionService::Stub *stub,
                           grpc::CompletionQueue *completion_queue,
                           MultiTransactionRequest *request, MultiTransactionResponse *response);

  ~EntityTransactionHandler() override = default;

  EntityTransactionHandler(const EntityTransactionHandler &) = delete;
  EntityTransactionHandler &operator=(const EntityTransactionHandler &) = delete;
  EntityTransactionHandler(EntityTransactionHandler &&) = delete;
  EntityTransactionHandler &operator=(EntityTransactionHandler &&) = delete;

protected:
  virtual void handleNewCallState() override;

  HandlerTag tag_;

  EntityTransactionService::Stub *stub_;

  std::unique_ptr<grpc::ClientAsyncResponseReader<MultiTransactionResponse>> rpc_;

  MultiTransactionRequest *request_;
  MultiTransactionResponse *response_;
};

} // namespace linkahead::transaction
