/*
 * This file is a part of the LinkAhead Project.
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021-2024 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 *********************************************************************************
 *
 * This is derived work which is heavily based on
 * https://github.com/NeiRo21/grpcpp-bidi-streaming, Commit
 * cd9cb78e5d6d72806c2ec4c703e5e856b223dc96, Aug 10, 2020
 *
 * The orginal work is licensed as
 *
 * > MIT License
 * >
 * > Copyright (c) 2019 NeiRo21
 * >
 * > Permission is hereby granted, free of charge, to any person obtaining a
 * > copy of this software and associated documentation files (the "Software"),
 * > to deal in the Software without restriction, including without limitation
 * > the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * > and/or sell copies of the Software, and to permit persons to whom the
 * > Software is furnished to do so, subject to the following conditions:
 * >
 * > The above copyright notice and this permission notice shall be included in
 * > all copies or substantial portions of the Software.
 * >
 * > THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * > IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * > FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * > AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * > LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * > FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * > DEALINGS IN THE SOFTWARE.
 */
#ifndef LINKAHEAD_UNARY_RPC_HANDLER_H
#define LINKAHEAD_UNARY_RPC_HANDLER_H

#include "linkahead/handler_interface.h"  // for HandlerTag, Handl...
#include "linkahead/transaction_status.h" // for TransactionStatus
#include <grpcpp/client_context.h>        // for ClientContext
#include <grpcpp/completion_queue.h>      // for CompletionQueue
#include <grpcpp/support/status.h>        // for Status

namespace linkahead::transaction {

class UnaryRpcHandler : public HandlerInterface {
public:
  inline UnaryRpcHandler(grpc::CompletionQueue *completion_queue)
    : HandlerInterface(), state_(CallState::NewCall), completion_queue(completion_queue) {}

  void Start() override {
    if (state_ == CallState::NewCall) {
      transaction_status = TransactionStatus::EXECUTING();
      OnNext(true);
    }
  }

  bool OnNext(bool ok) override;

  void Cancel() override;

protected:
  virtual void handleNewCallState() = 0;
  void handleCallCompleteState();

  enum class CallState { NewCall, CallComplete };
  CallState state_;
  grpc::CompletionQueue *completion_queue;

  grpc::ClientContext call_context;
  grpc::Status status_;
};

} // namespace linkahead::transaction

#endif
