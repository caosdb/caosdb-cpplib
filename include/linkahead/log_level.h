/*
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021-2024 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef LINKAHEAD_LOG_LEVEL_H
#define LINKAHEAD_LOG_LEVEL_H

#define LINKAHEAD_LOG_LEVEL_OFF 1000000
#define LINKAHEAD_LOG_LEVEL_FATAL 700
#define LINKAHEAD_LOG_LEVEL_ERROR 600
#define LINKAHEAD_LOG_LEVEL_WARN 500
#define LINKAHEAD_LOG_LEVEL_INFO 400
#define LINKAHEAD_LOG_LEVEL_DEBUG 300
#define LINKAHEAD_LOG_LEVEL_TRACE 200
#define LINKAHEAD_LOG_LEVEL_ALL 0

#ifndef LINKAHEAD_DEFAULT_LOG_LEVEL
#define LINKAHEAD_DEFAULT_LOG_LEVEL LINKAHEAD_LOG_LEVEL_ERROR
#endif

#endif
