/*
 * This file is a part of the LinkAhead Project.
 *
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021-2024 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef LINKAHEAD_PROTOBUF_HELPER_H
#define LINKAHEAD_PROTOBUF_HELPER_H

// IWYU pragma: no_include <google/protobuf/extension_set.h>
#include <google/protobuf/arena.h>          // for Arena
#include <google/protobuf/util/json_util.h> // for JsonOptions, MessageToJs...
#include <string>                           // for string

#define LINKAHEAD_DEBUG_MESSAGE_STRING(message, out)                                               \
  std::string out;                                                                                 \
  {                                                                                                \
    google::protobuf::util::JsonOptions options;                                                   \
    options.add_whitespace = true;                                                                 \
    google::protobuf::util::MessageToJsonString(message, &out, options);                           \
  }

namespace linkahead::utility {

using google::protobuf::Arena;

auto get_arena() -> Arena *;
auto reset_arena() -> void;

/**
 * Abstract wrapper class for Protobuf messages.
 */
template <typename P> class ProtoMessageWrapper {
public:
  virtual ~ProtoMessageWrapper() = 0;
  /**
   * Return a json representation of this object.
   */
  virtual inline auto ToString() const noexcept -> const std::string = 0;

  /**
   * Return true if the underlying Protobuf messages have the same
   * serialization.
   */
  inline auto operator==(const ProtoMessageWrapper<P> &other) const noexcept -> bool {
    if (this->wrapped != nullptr && other.wrapped != nullptr) {
      return this->wrapped->SerializeAsString() == other.wrapped->SerializeAsString();
    }
    // both nullptr?
    return this->wrapped == other.wrapped;
  }

  /**
   * Return true if the underlying Protobuf messages have a different
   * serialization.
   */
  inline auto operator!=(const ProtoMessageWrapper<P> &other) const noexcept -> bool {
    return !(*this == other);
  }

protected:
  inline static auto CopyProtoMessage(P *wrapped) -> P * {
    P *copy = Arena::CreateMessage<P>(get_arena());
    copy->CopyFrom(*wrapped);
    return copy;
  }
  ProtoMessageWrapper() : ProtoMessageWrapper(Arena::CreateMessage<P>(get_arena())) {}
  ProtoMessageWrapper(P *wrapped) : wrapped(wrapped) {}
  P *wrapped;
};

template <typename P> ProtoMessageWrapper<P>::~ProtoMessageWrapper() = default;

/**
 * Wrapper class for scalar Protobuf messages.
 *
 * Scalar means in this context, any message but classes derived from
 * RepeatedPtrField.
 */
template <typename P> class ScalarProtoMessageWrapper : public ProtoMessageWrapper<P> {
public:
  inline virtual ~ScalarProtoMessageWrapper() = default;
  inline ScalarProtoMessageWrapper() = default;
  inline ScalarProtoMessageWrapper(P *wrapped) : ProtoMessageWrapper<P>(wrapped) {}
  /**
   * Return a json representation of this object.
   */
  inline auto ToString() const noexcept -> const std::string override {
    if (this->wrapped == nullptr) {
      return "{}\n";
    }
    LINKAHEAD_DEBUG_MESSAGE_STRING(*this->wrapped, out)
    return out;
  }
};

} // namespace linkahead::utility
#endif
