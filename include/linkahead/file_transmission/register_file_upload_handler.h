/*
 * This file is a part of the LinkAhead Project.
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021-2024 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 *********************************************************************************
 *
 * This is derived work which is heavily based on
 * https://github.com/NeiRo21/grpcpp-bidi-streaming, Commit
 * cd9cb78e5d6d72806c2ec4c703e5e856b223dc96, Aug 10, 2020
 *
 * The orginal work is licensed as
 *
 * > MIT License
 * >
 * > Copyright (c) 2019 NeiRo21
 * >
 * > Permission is hereby granted, free of charge, to any person obtaining a
 * > copy of this software and associated documentation files (the "Software"),
 * > to deal in the Software without restriction, including without limitation
 * > the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * > and/or sell copies of the Software, and to permit persons to whom the
 * > Software is furnished to do so, subject to the following conditions:
 * >
 * > The above copyright notice and this permission notice shall be included in
 * > all copies or substantial portions of the Software.
 * >
 * > THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * > IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * > FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * > AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * > LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * > FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * > DEALINGS IN THE SOFTWARE.
 */
#ifndef LINKAHEAD_FILE_TRANSMISSION_REGISTER_FILE_UPLOAD_H
#define LINKAHEAD_FILE_TRANSMISSION_REGISTER_FILE_UPLOAD_H

#include "caosdb/entity/v1/main.pb.h"        // for FileTransmissionS...
#include "caosdb/entity/v1/main.grpc.pb.h"   // for FileDownloadResponse
#include "linkahead/handler_interface.h"     // for HandlerTag, Handl...
#include "linkahead/unary_rpc_handler.h"     // for  UnaryRpcHandler
#include <grpcpp/completion_queue.h>         // for CompletionQueue
#include <grpcpp/support/async_unary_call.h> // for ClientAsyncResponseReader
#include <memory>                            // for unique_ptr

namespace linkahead::transaction {

using caosdb::entity::v1::FileTransmissionService;
using caosdb::entity::v1::RegisterFileUploadRequest;
using caosdb::entity::v1::RegisterFileUploadResponse;

class RegisterFileUploadHandler final : public UnaryRpcHandler {
public:
  RegisterFileUploadHandler(HandlerTag tag, FileTransmissionService::Stub *stub,
                            grpc::CompletionQueue *completion_queue,
                            RegisterFileUploadRequest *request,
                            RegisterFileUploadResponse *response);

  ~RegisterFileUploadHandler();

  RegisterFileUploadHandler(const RegisterFileUploadHandler &) = delete;
  RegisterFileUploadHandler &operator=(const RegisterFileUploadHandler &) = delete;
  RegisterFileUploadHandler(RegisterFileUploadHandler &&) = delete;
  RegisterFileUploadHandler &operator=(RegisterFileUploadHandler &&) = delete;

protected:
  void handleNewCallState() override;

  HandlerTag tag_;

  FileTransmissionService::Stub *stub_;

  std::unique_ptr<grpc::ClientAsyncResponseReader<RegisterFileUploadResponse>> rpc_;

  RegisterFileUploadRequest *request_;
  RegisterFileUploadResponse *response_;
};

} // namespace linkahead::transaction

#endif
