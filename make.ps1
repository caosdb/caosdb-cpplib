param (
    [string]$target
)

$buildType = "Release"
$cppStd=17

function Install-Conan {
    Write-Output "Installing Release dependencies with Conan..."
        # check if conan is available
        if (-not (Get-Command conan -ErrorAction SilentlyContinue)) {
            Write-Output "Conan is not available. Please install Conan or activate the Conan environment venv"
            exit 1
        }
        conan install . --build=missing -s build_type=$buildType -s compiler.cppstd=$cppStd 
        cmake --preset conan-default
}

function Invoke-Build {
    Write-Output "Building the project..."
    # check if msbuild is available
    if (-not (Get-Command msbuild -ErrorAction SilentlyContinue)) {
        Write-Output "msbuild is not available. Please install Visual Studio or open the Developer PowerShell."
        exit 1
    }
    # check if conan install was run
    if (-not (Test-Path .\build\liblinkahead.sln)) {
        Write-Output "Please run conan-install first."
        exit 1
    }
    msbuild .\build\liblinkahead.sln /property:Configuration=$buildType
}

function Invoke-Tests {
    Write-Output "Running tests..."
    # check if build was run before
    if (-not (Test-Path .\build\Release\linkahead.dll)) {
        Write-Output "Please build the project first."
        exit 1
    }
    Set-Location .\build\
    ctest
    Set-Location ..\
}

function Invoke-Create {
    Write-Output "Creating Conan package..."
    conan create . 
}

# check if vswhere is available
if (-not (Test-Path "C:\Program Files (x86)\Microsoft Visual Studio\Installer\vswhere.exe")) {
    Write-Output "vswhere is not available. Please install Visual Studio"
    exit 1
}

# locate the latest Visual Studio installation
$currentPath = Get-Location
$installPath = &"C:\Program Files (x86)\Microsoft Visual Studio\Installer\vswhere.exe" -latest -products * -requires Microsoft.Component.MSBuild -property installationPath
Import-Module (Join-Path $installPath "Common7\Tools\Microsoft.VisualStudio.DevShell.dll")
Enter-VsDevShell -VsInstallPath $installPath
Set-Location $currentPath


# Windows is with Release only for now

switch ($target) {
    "build" {
        Invoke-Build
    }
    "test" {
        Invoke-Tests
    }
    "conan-install" {
        Install-Conan
    }
    "conan-create" {
        Invoke-Create
    }
    "clean"{
        Write-Output "Cleaning the project..."
        Remove-Item -Recurse -Force .\build\*
    }
    "all" {
        Install-Conan
        Invoke-Build
        Invoke-Tests
        Invoke-Create
    }
    default {
        Write-Output "Usage: .\make.ps1 [all|conan-install|conan-create|build|test|clean]"
    }
}
