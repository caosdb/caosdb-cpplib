# Release Guidelines for the LinkAhead C++ Client Library

This document specifies release guidelines in addition to the generel release
guidelines of the LinkAhead Project
([RELEASE_GUIDELINES.md](https://gitlab.com/caosdb/caosdb/blob/dev/RELEASE_GUIDELINES.md))

## General Prerequisites

* All tests are passing.
* FEATURES.md is up-to-date and a public API is being declared in that document.
* CHANGELOG.md is up-to-date.
* DEPENDENCIES.md is up-to-date (e.g. server version)

## Steps

1. Create a release branch from the dev branch. This prevents further changes to
   the code base and a never ending release process. Naming:
   `release-<VERSION>`. Also create a branch with the same name in cppinttests.

2. Update CHANGELOG.md

3. Check all general prerequisites.

4. Update version numbers:
   1. In [CMakeLists.txt](./CMakeLists.txt): Check the version variables and
      make sure that the compatible caosdb-server version is set correctly.
   2. In `conanfile.py` and `vcpkg.json`: Update the `version` variable.
   3. In `linkahead-cppinttest/conanfile.py`: Update the version

5. Merge the release branch into the main branch.

6. Tag the latest commit of the main branch with `v<VERSION>`. Push the tag and the main branch.

7. Create release in Gitlab:
   1. On [Releases](https://gitlab.indiscale.com/caosdb/src/caosdb-cpplib/-/releases), click "New release"
   2. Choose tag, type release title (same as tag), no milestone, possibly short release note, no
      manual assets are necessary, possibly link to documentation.

8. Delete the release branch.

9. Release cppinttests with updated cpplib version

10. Merge `main` back into `dev` and increase patch version by one to begin next
    release cycle.
    * update `conanfile.py`
    * update `CMakeLists.txt`
    * update `CHANGELOG.md` with empty `[Unreleased]` sections.
    * Don't forget to update cppintest.
